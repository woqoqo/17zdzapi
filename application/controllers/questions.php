<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Questions extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}

	public function send() {
		$this->form_validation->set_rules('answerer[]', 'Answerer', 'required|is_natural_no_zero');
		$this->form_validation->set_rules('content', 'Content', 'required');

		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '50001');
		}
        
		//answer
		$tmp_answerer_id = $this->input->post('answerer');
		$answerer_id = array();

		foreach($tmp_answerer_id as $k => $v) 
		{
			if($k > 5)
			{
				break;
			}

			$answerer_id[] = $v;
		}

		if(count($answerer_id) == 0)
		{
			show_ajax_error(validation_errors(), '50001');
		}
        
		//questions
        $images = $this->input->post('images', true);
        
        if (is_array($images) && count($images))
        {
            $data['images'] = serialize($images);
        }
        
		$content = $this->input->post('content');
		$dateline = (int) $this->input->post('timestamp');
        
		$data['dateline'] = $dateline;
		$data['uid'] = $this->uid;
		$data['username'] = $this->username;
		$data['content'] = mb_substr($content, 0, 50);
        
		$city_name = $this->input->get_post('city_name');

		if ($city_name) {
			$data['city_name'] = $city_name;
		}

		$country_name = $this->input->get_post('country_name');

		if ($country_name) {
			$data['country_name'] = $country_name;
		}

		$this->load->model('Questions_model');
        $this->load->model('Messages_model');
        $this->load->model('Message_items_model');
                
		$question_id = $this->Questions_model->insert($data);

		if ( ! $question_id)
		{
			show_ajax_error('Question insert false', '50003');
		}

		//answerer insert data
		$this->load->model('Question_answers_model');
		$answerer_insert_data = array();

		foreach ($answerer_id as $v)
        {
            $answerer_insert_data[] = array(
                'did' => $question_id,
                'uid' => $this->uid,
                'username' => $this->username,
                'answerer_uid' => $v,
                'dateline' => $dateline
            );
           
        }
      

		$this->Question_answers_model->insert_batch($answerer_insert_data);		

		//return result
        $data = array(
            'dateline' => $dateline - 1,
            'uid' => $this->uid,
            'username' => $this->username
        );
        
		show_ajax_success(array('data' => $data));
	}

	
    
    private function _question_item_info($filter)
    {        
        $this->load->model('Question_answers_model');
		$select = 'questions.id, 
            questions.uid, 
            questions.username, 
            questions.content, 
            questions.images, 
            questions.dateline, 
            questions.city_name, 
            questions.country_name,
            question_answers.answerer_uid,
            question_answers.status';

        $this->db->join('questions', 'questions.id = question_answers.did');
		
		$question = $this->Question_answers_model->get_one($select, $filter);

        if ($question) {
            $question->avatar = avatar_url($question->uid, '100');
            $question->normal_avatar = avatar_url($question->uid);
            $question->answerer_username = $this->username;

            if ($question->images) {
                $images = array();
                $image_array = unserialize($question->images);
                
                foreach ($image_array as $v) {
                    $images[] = $v;
                }

                $question->images = $images;
            }
            
        } else {
            show_ajax_error('Question does not exist', '50011');
        }
        
        return $question;
    }
    
    public function item_info()
    {
        $id = (int) $this->input->POST('id');
        
        if ($id <= 0) {
			show_ajax_error('Questions id is not legitimate', '50009');
		}
        
        $filter = array(    
            'question_answers.did' => $id
		);
       
        $question = $this->_question_item_info($filter);
        
        $user = array();
        
        if ($question) {
            $user = $this->_user_info($question->uid);
        }
        
        $result = array(
            'question' => $question,
            'user' => $user
        );
        
        show_ajax_success($result);
    }

    public function received() {        
		$filter = array(
			'question_answers.answerer_uid' => $this->uid,
            'question_answers.status' => QUESTION_DEFAULT
		);
               
        $question = $this->_question_item_info($filter);
        $user = array();
        
        if ($question) {
            $user = $this->_user_info($question->uid);
        }
        
        $result = array(
            'question' => $question,
            'user' => $user
        );
        
        show_ajax_success($result);
	}
	
	public function accept() {
        
        $id = (int) $this->input->POST('id');
        
        if ($id <= 0) {
			show_ajax_error('Questions id is not legitimate', ' ');
		}        
        
        $filter = array(
            'question_answers.did' => $id,
			'question_answers.answerer_uid' => $this->uid,
            'question_answers.status' => QUESTION_DEFAULT
		);
       
        $question = $this->_question_item_info($filter);
        
        $user = array();
        
        if ($question) {
            //根据question 生成一个新的群组
            $this->load->model('Groups_model');
            $this->load->model('Group_users_model');
            
                
            $group_filter = array('data_id' => $id);
            $group = $this->Groups_model->get_one('id', $group_filter);
            
            $group_id = 0;
            
            $dateline = dateline();
            
            //判断这个群存在不存在， 不存在就添加一个新群
            if (! $group) {
                $group_insert_data = array(
                    'uid' => $question->uid,
                    'username' => $question->username,
                    'data_id' => $question->id,
                    'group_name' => '',
                    'dateline' => $dateline
                );
                
                $group_id = $this->Groups_model->insert($group_insert_data);
                
                $group_users_insert_data = array();
                $group_users_insert_data[] = array(
                    'uid' => $question->uid,
                    'username' => $question->username,
                    'group_id' => $group_id,
                    'dateline' => $dateline
                );
                
                $group_users_insert_data[] = array(
                    'uid' => $this->uid,
                    'username' => $this->username,
                    'group_id' => $group_id,
                    'dateline' => $dateline
                );
                
                
                log_var($group_users_insert_data);
                
                $this->Group_users_model->insert_batch($group_users_insert_data);
                
                log_message('error', $this->db->last_query());
                log_message('error', '--------> Group_users_model insert');
                
                
            } else {
                $group_id = $group->id;
                
                $group_users_insert_data = array(
                    'uid' => $this->uid,
                    'username' => $this->username,
                    'group_id' => $group_id,
                    'dateline' => $dateline
                );
                
                $this->Group_users_model->insert($group_users_insert_data);
            }            
            
            //提问者的User信息
            $user = $this->_user_info($question->uid);

            //插入消息
            $data = array();

            //群组ID
            $data['group_id'] = $group_id;
            
            //接收者 uid
            $data['other_uid'] = $this->uid;

            //接收者 username
            $data['other_username'] = $this->username;

            //问题发送者 
            $data['uid'] = $question->uid;

            //问题发送者 username 
            $data['username'] = $question->username;

            //消息内容
            $data['content'] = $question->content;

            //消息类型： 1普通文本消息, 2图片, 3问题, 4产品, 5地理位置, 6语音
            $data['data_type'] = MESSAGE_TYPE_QUESTION;

            //问题的ID
            $data['data_id'] = $question->id;

            //关联文件的名称
            $data['filename'] = (string) $this->input->post('filename');
        
            //录音的名称, 长度
            $data['duration'] = 0;
            
            //图片的高度,宽度
            $data['image_width'] = (float) $this->input->post('image_width');
            $data['image_height'] = (float)  $this->input->post('image_height');

            //消息的状态
            $data['status'] = MESSAGE_UNREAD;
            
            //关联的问题
            $data_source = array(
                'images' => $question->images,
                'city_name' => $question->city_name,
                'country_name' => $question->country_name,
                'dateline' => $question->dateline
            );
            
            $data['data_source'] = json_encode($data_source);

            //引入 Message_items_model
            $this->load->model('Message_items_model');

            //插入数据
            $this->Message_items_model->insert_message($data);
            
            //读取新消息
            $question = $this->_deal_quesition(QUESTION_ACCEPT);
        }
        
        $result = array(
            'question' => $question,
            'user' => $user
        );
        
        show_ajax_success($result);
	}

	public function refuse()
	{
        $question = $this->_deal_quesition(QUESTION_CANCEL);
        $user = $this->_user_info($question->uid);
        
        $result = array(
            'question' => $question,
            'user' => $user
        );
        
        show_ajax_success($result);
	}

	private function _deal_quesition($accepted)
	{
		$id = (int) $this->input->POST('id');
        
        if ($id <= 0) {
			show_ajax_error('Questions id is not legitimate', '50009');
		}
     
		$update = array(
			'status' => $accepted
		);

        $update_filter = array(
            'did' => $id,
            'answerer_uid' => $this->uid
        );
        
        $this->load->model('Question_answers_model');
        
		$this->Question_answers_model->update($update, $update_filter);
                
        
        $filter = array(
            //'question_answers.did' => $id,
			'question_answers.answerer_uid' => $this->uid,
            'question_answers.status' => QUESTION_DEFAULT
		);
       
        $question = $this->_question_item_info($filter);
        
        return $question;
	}
    
    private function _user_info($uid) {
        $this->load->model('User_info_model');
        
        $user_filter = array(
            'uid' => $uid
        );
        
        $user_select = 'uid, username, gender, country_name, city_name';
        
        $user = $this->User_info_model->get_one($user_select, $user_filter);
        
        $user->place_info =  $user->country_name .  $user->city_name;
        $user->gender = $this->User_info_model->get_gender_name_by_key( $user->gender);

        //是否关注
        $this->load->model('Contacts_model');

        $follow_filter = array(
            'uid' => $this->uid,
            'other_uid' => $uid
        );

        $count = $this->Contacts_model->count_all($follow_filter);
                
        $user->following = $count > 0 ? TRUE : FALSE;

        return $user;
    }

}

// END Questions class

/* End of file questions.php */
/* Location: ./application/controllers/questions.php */