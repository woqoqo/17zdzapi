<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Footprints_model extends MY_Model {

	public $_table = 'footprints';

	public function __construct()
	{
		parent::__construct();
	}
}