<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Groups extends MY_Controller {

    function __construct() {
        parent::__construct();
    }
    
    public function info() 
    {
        //群组的id
        $this->form_validation->set_rules('group_id', 'Group id', 'required|is_natural_no_zero');
        $group_id = $this->input->post('group_id', TRUE);

        //验证POST数据
        if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '100001');
        }
                
        $this->load->model('Groups_model');
        
        $group_select = 'uid, username, dateline';
        
        $group_filter = array(
            'id' => $group_id
        );
        
        $group = $this->Groups_model->get_one($group_select, $group_filter);
        
        if (! $group) {
             show_ajax_error('Group does not exist', '100002');
        }
        
        //users
        $this->load->model('Group_users_model');
        
        $user_select = 'uid, username, dateline';
        $user_filter = array(
            'group_id' => $group_id,
            'uid !=' => $this->uid
        );
        
        $users = $this->Group_users_model->get_list($user_select, $user_filter);
        
        show_ajax_success(array(
            'group' => $group,
            'users' => $users
        ));
    }
    
    public function add_user()
    {
        //群组的id
        $this->form_validation->set_rules('group_id', 'Group id', 'required|is_natural_no_zero');
        $group_id = $this->input->post('group_id', TRUE);

        //验证POST数据
        if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '100011');
        }
        
        //传过来的uid
        $tmp_uid = $this->input->post('uid');
        
        if (! is_array($tmp_uid) || count($tmp_uid) == 0) {
            show_ajax_error('No uid', '100012');
        } 
        
        //群组成员
        $this->load->model('Group_users_model');
        $group_user_select = 'uid';
        $group_user_filter = array(
            'group_id' => $group_id
        );
        
        $group_users = $this->Group_users_model->get_list($group_user_select, $group_user_filter);
        if (! $group_users) {
            show_ajax_error('Group does not exist', '100013');
        }
        
        $group_user_id = array();
        foreach($group_users as $v)
        {
            $group_user_id[] = $v->uid;
        }
        
        $group_user_count = count($group_user_id);
        
        if ($group_user_count >= GROUP_USER_MAXIMUM) {
            show_ajax_error('Group must have 6 user', '100014');
        }
        
        //通讯录所有的人
        $this->load->model('Contacts_model');

        $filter = array(
            'uid' => $this->uid,
            'other_user_type' => USER_TYPE_NORMAL
        );
        
        $select = 'other_uid, other_username';
        $user_list = $this->Contacts_model->get_list($select, $filter);
        
        if (empty($user_list)) {
            show_ajax_error('Contacts no data', '100032');
        }
        
        $users = array();

        foreach($user_list as $v)
        {
            $users[$v->other_uid] = $v;
        }
        
        $group_insert_data = array();
        $dateline = dateline();
        
        //通讯里是否存在这个用户， 是否加入这个群
        $i = 1;
        foreach ($tmp_uid as $v) {
            if ($v > 0 &&  isset($users[$v]) && (! in_array($v, $group_user_id))) 
            {
                if (($group_user_count + $i) > GROUP_USER_MAXIMUM) {
                    break;
                }   
                
                $group_insert_data[] = array(
                    'uid' => $users[$v]->other_uid,
                    'username' => $users[$v]->other_username,
                    'group_id' => $group_id,
                    'dateline' => $dateline
                );  
                
                $i ++;
            }// end if
        } //  end foreach
        
        if ($group_insert_data) {
            $this->Group_users_model->insert_batch($group_insert_data);
        }
        
        show_ajax_success(array('users' => $group_insert_data));
    }
    
    public function remove_user()
    {
        //群组的id
        $this->form_validation->set_rules('group_id', 'Group id', 'required|is_natural_no_zero');
        //删除对象 uid
        $this->form_validation->set_rules('uid', 'uid', 'required|is_natural_no_zero');
        
        //验证POST数据
        if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '100031');
        }
        
        $uid = $this->input->post('uid', TRUE);
        $group_id = $this->input->post('group_id', TRUE);
        
        $this->load->model('Groups_model');
        $count_filter = array(
            'id' => $group_id,
            'uid' => $this->uid
        );
        $count = $this->Groups_model->count_all($count_filter);
        
        log_message('error', $this->db->last_query());
        
        if ($count == 0) {
            show_ajax_error(validation_errors(), '100032');
        }
        
        $this->load->model('Group_users_model');
        $filter = array(
            'uid' => $uid,
            'group_id' => $group_id
        );
        $this->Group_users_model->delete($filter);

    }
    
    
       public function create_ptp_group()
    {
        $this->form_validation->set_rules('other_uid', 'other_uid', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('other_username', 'other_username', 'required');
        
        //验证POST数据
        if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '100024');
        }
        
        $this->create_group();        
    }
    
    public function create_group()
    {               
        $other_uid = $this->input->post('other_uid');
        $other_username = $this->input->post('other_username');
        
        //传过来的uid
        $tmp_uid = $this->input->post('uid');
        
        if (! is_array($tmp_uid) || count($tmp_uid) == 0) {
            show_ajax_error('No uid', '100022');
        }
                
        //通讯录所有的人
        $this->load->model('Contacts_model');

        $filter = array(
            'uid' => $this->uid,
            'other_user_type' => USER_TYPE_NORMAL
        );
        
        $select = 'other_uid, other_username';
        $user_list = $this->Contacts_model->get_list($select, $filter);
        
        if (empty($user_list)) {
            show_ajax_error('Contacts no data', '100032');
        }
        
        //$group_id
        $users = array();

        foreach($user_list as $v)
        {
            $users[$v->other_uid] = $v;
        }
        
        $group_insert_data = array();
        $dateline = dateline();
        
        //通讯里是否存在这个用户
        $i = 1;
        foreach ($tmp_uid as $v) {
            if ($v > 0 && isset($users[$v])) 
            {
                $group_insert_data[] = array(
                    'uid' => $users[$v]->other_uid,
                    'username' => $users[$v]->other_username,
                    //'group_id' => $group_id,
                    'dateline' => $dateline
                );
                
                $i ++;
            }// end if
        } //  end foreach
        
        if ($group_insert_data) 
        {
            $this->load->model('Groups_model');
            $this->load->model('Group_users_model');
            
            $group_data = array(
                'uid' => $this->uid,
                'username' => $this->username,
                'data_id' => 0,
                'group_name' => '',
                'dateline' => $dateline
            );

            $group_id = $this->Groups_model->insert($group_data);
            
            foreach($group_insert_data as $k => $v)
            {
                $group_insert_data[$k]['group_id'] = $group_id;
            }
            
            //把自己也加进去
            $group_insert_data[] = array(
                'uid' => $this->uid,
                'username' => $this->username,
                'dateline' => $dateline,
                'group_id' => $group_id
            );
            
            //对方的uid, 没有做验证用户
            if ($other_uid) {
                $group_insert_data[] = array(
                    'uid' => $other_uid,
                    'username' => $other_username,
                    'dateline' => $dateline,
                    'group_id' => $group_id
                );
            }
            else {
                $other_uid = $group_insert_data[0]['uid'];
                $other_username = $group_insert_data[0]['username'];
            }
            
            $this->Group_users_model->insert_batch($group_insert_data);
                        
            $flag = substr(md5($group_id), 0, 6);
            
            show_ajax_success(array('group'=> array(
                'group_id' => "$group_id",
                'uid' => "$other_uid",
                'username' => $other_username,
                'flag' => $flag,
                'group_name' => '')));
        }
        else {
            show_ajax_error();
        }
    }
    
    
 
}