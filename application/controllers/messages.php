<?php if (!defined('BASEPATH'))  exit('No direct script access allowed');

class Messages extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }
    
    // --------------------------------------------------------------------

    public function send() 
    {
        
        $data = array();
        
        //发送者
        $data['uid'] = $this->uid;
        
        //发送者 username
        $data['username'] = $this->username;

        //接收者 uid
        $this->form_validation->set_rules('other_uid', 'User id', 'required|is_natural_no_zero');
        $data['other_uid'] = $this->input->post('other_uid', TRUE);

        //接收者 username
        $this->form_validation->set_rules('other_username', 'Username', 'required');
        $data['other_username'] = $this->input->post('other_username', TRUE);

        //消息内容
        $this->form_validation->set_rules('content', 'Content', 'required');
        $data['content'] = $this->input->post('content', TRUE);

        //验证POST数据
        if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '60001');
        }
        
        //消息类型： 1普通消息, 2群组, 3问题, 4产品
        $data['data_type'] = (int) $this->input->post('data_type', TRUE);
        
        //位置
        if ($data['data_type'] == MESSAGE_TYPE_LOCATION) {
        
            $location = array(
                'country' => $this->input->post('country', TRUE),
                'country_code' => $this->input->post('country_code', TRUE),
                'name' => $this->input->post('name', TRUE),
                'state' => $this->input->post('state', TRUE),
                'sub_locality' => $this->input->post('sub_locality', TRUE),
                'thoroughfare' => $this->input->post('thoroughfare', TRUE),
                'latitude' => $this->input->post('latitude', TRUE),
                'longitude' => $this->input->post('longitude', TRUE)
            );
            
            //关联数据内容
            $data['data_source'] = json_encode($location); 
        }
        
        //关联数据ID， 如问题ID, 产品ID
        $data['data_id'] = (int) $this->input->post('data_id', TRUE);
        
        $data['group_id'] = (int) $this->input->post('group_id', TRUE);
       
        //录音的名称, 长度        
        $data['duration'] = (float) $this->input->post('duration', TRUE);
        
        //图片的高度,宽度
        $data['image_width'] = (float) $this->input->post('image_width');
        $data['image_height'] =  (float) $this->input->post('image_height');
        
        //关联文件的名称
        $data['filename'] = (string) $this->input->post('filename', TRUE);
       
        //消息的状态
        $data['status'] = MESSAGE_READ;

        //引入 Message_items_model
        $this->load->model('Message_items_model');
        
        //插入数据
        $message = $this->Message_items_model->insert_message($data);
        // --------------- 返回的数据
        
        //是否为自己的消息
        $message['is_mine'] = TRUE;
        $message['data_url'] = fetch_user_data_path($message['filename']);
        
        log_var($message);

        show_ajax_success(array('data' => $message));
    }

    // --------------------------------------------------------------------

    public function sync() {        
        //接收者 uid
        $this->form_validation->set_rules('flag', 'flag', 'required');
        
        //验证POST数据
        if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '60005');
        }
        
        $flag = $this->input->post('flag', TRUE);

        //引入 Message_items_model
        $this->load->model('Message_items_model');
                
        $dateline = dateline();

        //过滤条件
        $filter = array(
            'uid' => $this->uid,
            'flag' => $flag,
            'status' => MESSAGE_UNREAD,
            'dateline <=' => $dateline
        );
  
        //select
        $select = 'id, flag, uid ,username, other_uid, other_username, sender_uid, 
            content, dateline, data_type, data_id, data_source, filename, duration, 
            image_width, image_height';
        
        $this->db->query('SET NAMES utf8mb4');
        $this->db->order_by('id ASC');

        //新消息列表
        $data = $this->Message_items_model->get_list($select, $filter, UNLIMIT);
        
        //整理消息数据
        if ($data) {
            foreach ($data as $k => $v) {
                $data[$k]->is_mine = $v->sender_uid == $this->uid ? TRUE : FALSE;
                $data[$k]->data_url = fetch_user_data_path($v->filename);
            }
            
            //更改消息状态
            $update_data = array(
                'status' => MESSAGE_READ
            );
            
            $this->Message_items_model->update($update_data, $filter);
        }
        
        show_ajax_success(array('data' => $data));
    }
    
    // --------------------------------------------------------------------
    
    public function remove()
    {
        $this->form_validation->set_rules('id', 'Message id', 'required|is_natural_no_zero');
        
        //验证POST数据
        if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '60010');
        }
        
        $id = $this->input->post('id');

        $this->load->model('Message_items_model');
        
        $filter = array(
            'id' => $id,
            'uid' => $this->uid
        );
        $this->Message_items_model->delete($filter);
        
        show_ajax_success();
    }
    
    // --------------------------------------------------------------------
    
    public function remove_all()
    {
        log_var($_POST);
        
        $this->form_validation->set_rules('flag', 'flag', 'required');
        
        //验证POST数据
        if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '60110');
        }
        
        $flag = $this->input->post('flag');

        $this->load->model('Message_items_model');
        
        $filter = array(
            'flag' => $flag,
            'uid' => $this->uid
        );
        
        $this->Message_items_model->delete($filter);
        
        
        log_message('error', $this->db->last_query());
        
        show_ajax_success();
    }
    
    // --------------------------------------------------------------------

    public function check() {
        $data = array();

        //引入 Question_answers_model
        $this->load->model('Question_answers_model');

        $answer_filter = array(
            'answerer_uid' => $this->uid,
            'status' => QUESTION_NOT_ADOPTE
        );

        $data['question_count'] = $this->Question_answers_model->count_all($answer_filter);

        //引入 Message_items_model
        $this->load->model('Message_items_model');

        $message_select = 'COUNT(*) AS unread, uid, username, other_uid, 
            other_username, sender_uid, content, flag, group_id, dateline';
        
        $this->db->where('uid', $this->uid);
        $this->db->where('status', MESSAGE_UNREAD);
        $this->db->group_by('flag');
        $this->db->order_by('id', 'DESC');

        $messages = $this->Message_items_model->get_list($message_select);

        $data['messages'] = $messages;

        show_ajax_success($data);
    }

    // --------------------------------------------------------------------
    
    public function history() {
        //接收者 uid
        $this->form_validation->set_rules('flag', 'flag', 'required');
        
        //验证POST数据
        if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '60007');
        }
        
        $flag = $this->input->post('flag');

        //引入 Message_items_model
        $this->load->model('Message_items_model');
                
        $dateline = (int) $this->input->post('dateline');

        //过滤条件
        $filter = array(
            'uid' => $this->uid,
            'flag' => $flag
        );
        
        //limit
        $limit = 5;
        
        //offset
        $offset = 0;
        
        if ($dateline <= 0) {
            $dateline = dateline();
        }
        
        $filter['dateline <'] = $dateline;

        //select
        $select = 'id, flag, uid ,username, other_uid, other_username, sender_uid, 
            content, dateline, data_type, data_source, data_id, group_id, filename, 
            duration, image_width, image_height';        
        
        $this->db->query('SET NAMES utf8mb4');        
        $this->db->order_by('id DESC');

        //新消息列表
        $data = $this->Message_items_model->get_list($select, $filter, $limit, $offset);
                
        log_message('error', $this->db->last_query());
        
        //整理消息数据
        if ($data) {
            foreach ($data as $k => $v) {
                $data[$k]->is_mine = $v->sender_uid == $this->uid ? TRUE : FALSE;
                $data[$k]->data_url = fetch_user_data_path($v->filename);
            }   
        }
        
        log_message('error', '-----> history');
        
        show_ajax_success(array('data' => $data));
    }
    
    public function favorite()
    {
        
       //接收者 uid
        $this->form_validation->set_rules('id', 'id', 'required|is_natural_no_zero');
        
        //验证POST数据
        if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '60007');
        }
                
        $id = (int) $this->input->get_post('id', TRUE);
        
        $this->load->model('Message_items_model');
        
        $filter = array(
            'uid' => $this->uid,
            'id' => $id
        );
        
        $count = $this->Message_items_model->count_all($filter);
                
        if ($count) {
            $data = array();
            $data['uid'] = $this->uid;
            $data['message_id'] = $id;
            $data['dateline'] = dateline();
            
            $this->load->model('Message_favorite_model');
            $this->Message_favorite_model->insert($data);
            
            show_ajax_success();
        }
        else {
            show_ajax_error();
        }
    }
    
    public function favorited()
    {
        $this->load->model('Message_favorite_model');
        
        $filter = array(
            'message_favorite.uid' => $this->uid
        );            

		$select = 'message_items.id, 
            message_items.flag,
            message_items.other_uid, 
            message_items.other_username, 
            message_items.sender_uid, 
            message_items.content, 
            message_items.data_type, 
            message_items.data_source, 
            message_items.data_id, 
            message_items.filename, 
            message_items.duration, 
            message_items.image_width, 
            message_items.image_height, 
            message_favorite.dateline, 

           ';

        $this->db->join('message_items', 'message_favorite.message_id = message_items.id');
		$this->db->order_by('message_favorite.id', 'DESC');
        
		$messages = $this->Message_favorite_model->get_list($select, $filter);
        
        if ($messages) 
        {
            foreach ($messages as $k => $v) {
                $messages[$k]->data_url = fetch_user_data_path($v->filename);
            }   
        
            show_ajax_success(array('data' => $messages));
        }
        else {
            show_ajax_error();
        }
    }
    
    public function forward()
    {
        //接收者 uid
        $this->form_validation->set_rules('id', 'id', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('other_uid', 'other_uid', 'required|is_natural_no_zero');
        $this->form_validation->set_rules('other_username', 'other_username', 'required');
        
        //验证POST数据
        if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '60020');
        }
        
        $id = $this->input->get_post('id');
        $other_uid = $this->input->get_post('other_uid');
        $other_username = $this->input->get_post('other_username');

        $this->load->model('Message_items_model');
        $this->db->where('id', $id);
        $this->db->query('SET NAMES utf8mb4');
        
        $select = 'uid, other_uid, content, filename, data_type, data_id, 
            data_source, duration, image_width, image_height';
        
        $this->Message_items_model->set_result_type('array');
        $data = $this->Message_items_model->get_one($select);
                
        if (! $data) {
            show_ajax_error('No message', '60021');
        }
        
        if ($data['uid'] != $this->uid && $data['other_uid'] != $this->uid) 
        {
            show_ajax_error('No message: no my message', '60022');
        }
        
        //发送者
        $data['uid'] = $this->uid;
        
        //发送者 username
        $data['username'] = $this->username;

        //接收者 uid
        $data['other_uid'] = $other_uid;

        //接收者 username
        $data['other_username'] = $other_username;

        //消息的状态
        $data['status'] = MESSAGE_UNREAD;

        //引入 Message_items_model
        $this->load->model('Message_items_model');
        
        //插入数据
        $message = $this->Message_items_model->insert_message($data);
        // --------------- 返回的数据
        
        //是否为自己的消息
        $message['is_mine'] = TRUE;
        $message['data_url'] = fetch_user_data_path($message['filename']);
                
        show_ajax_success();
    }
    
    public function forward_batch()
    {
        //message item id
        $this->form_validation->set_rules('id', 'id', 'required|is_natural_no_zero');
        
        //验证POST数据
        if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '60033');
        }

        $tmp_uid = $this->input->post('uid');
        
        if (! is_array($tmp_uid) || count($tmp_uid) == 0) {
            show_ajax_error('No uid', '60030');
        }
        
        //通讯录所有的人
        $this->load->model('Contacts_model');

        $filter = array(
            'uid' => $this->uid,
            'other_user_type' => USER_TYPE_NORMAL
        );
        
        $select = 'other_uid, other_username';
        $user_list = $this->Contacts_model->get_list($select, $filter);
        $users = array();

        foreach($user_list as $v)
        {
            $users[$v->other_uid] = $v;
        }
        
        $message_id = $this->input->get_post('id');
        
        $this->load->model('Message_items_model');
        $this->db->where('id', $message_id);
        $this->db->query('SET NAMES utf8mb4');
        
        $message_item_select = 'uid, other_uid, content, filename, data_type, data_id, 
            data_source, group_id, duration, image_width, image_height';
        
        $message = $this->Message_items_model->get_one($message_item_select);
                
        if (! $message) {
            show_ajax_error('No message', '60021');
        }
        
        if ($message->uid != $this->uid && $message->other_uid != $this->uid) 
        {
            show_ajax_error('No message: no my message', '60022');
        }
   
        $insert_data = array();
        $dateline = dateline();
        
        
		foreach ($tmp_uid as $v) {
            if ($v > 0 &&  isset($users[$v])) {
                                
                $other_uid = (int) $v;
                $flag = $this->Message_items_model->message_flag($other_uid, $this->uid);
                
                $insert_data[] = array(
                    'flag' => $flag,
                    'content' => $message->content,
                    'uid' => $this->uid,
                    'username' => $this->username,
                    'other_uid' => $other_uid,
                    'other_username' => $users[$v]->other_username,
                    'sender_uid' => $this->uid,
                    'dateline' => $dateline,
                    'filename' => $message->filename,
                    'duration' => $message->duration,
                    'image_width' => $message->image_width,
                    'image_height' => $message->image_height,
                    'data_type' => $message->data_type,
                    'data_id' => $message->data_id,
                    'data_source' => $message->data_source,
                    'status' => MESSAGE_UNREAD
                );

                $insert_data[] = array(
                    'flag' => $flag,
                    'content' => $message->content,
                    'uid' => $other_uid,
                    'username' => $users[$v]->other_username,
                    'other_uid' => $this->uid,
                    'other_username' => $this->username,
                    'sender_uid' => $this->uid,
                    'dateline' => $dateline,
                    'filename' => $message->filename,
                    'duration' => $message->duration,
                    'image_width' => $message->image_width,
                    'image_height' => $message->image_height,
                    'data_type' => $message->data_type,
                    'data_id' => $message->data_id,
                    'data_source' => $message->data_source,
                    'status' => MESSAGE_UNREAD
                );
            }// end if
            
        } //  end foreach
        
        
        if ($insert_data) {
            $this->Message_items_model->insert_batch($insert_data);
        }
        
        show_ajax_success();
        
    }//end function
}