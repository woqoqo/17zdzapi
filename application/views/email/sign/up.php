<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title></title>
    </head>
    <body>
        <div style="border:1px solid #c8cfda;padding:40px">
            <p style="font-size:16px;margin:5px 0">尊敬的用户，您好！</p>
            <p>
                欢迎使用一起找地主，您的账号已经<span class="il">注册</span><span class="il">成功</span>。
            </p>
            <p>
                请点击下面的链接完成账号激活: <br>
                    <a href="http://api.17zdz.com/home/active.html?code=<?php echo $activation; ?>" title="账号激活">
                        http://api.17zdz.com/home/active.html?code=<?php echo $activation; ?>
                    </a>
            </p>
            <p>
                如果以上链接无法点击，请将该链接复制到浏览器（如chrome）的地址栏中访问，也可以成功完成账号激活！
            </p>
            <p>
                管理帐户信息如下，请妥善保管。<br />
                管理帐户：<b><?php echo $username; ?></b> <br />			
                密保邮箱：<b><a href="mailto:<?php echo $email; ?>" target="_blank">
                        <?php echo $email; ?></a></b>&nbsp;(<wbr>用于找回密码)<br />
                    登录地址：<a href="http://www.17zdz.com/sign/in.html" target="_blank">http://www.17zdz.com/sign/in.html</a><br />
            </p>	
            <div style="padding-v:20px">
                <p>一起找地主运营团队</p>					
            </div>
            <div style="border-top:1px solid #ccc;padding:6px 0;font-size:12px;margin:6px 0 20px">
                一起找地主：<a href="http://www.17zdz.com" target="_blank">http://www.17zdz.com</a><br />
            </div>
        </div>
    </body>
</html>

