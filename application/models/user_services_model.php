<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_services_model extends MY_Model {

	public $_table = 'user_services';

	function __construct()
	{
		parent::__construct();
	}

}

// END User_services_model class

/* End of file user_services_model.php */
/* Location: ./application/model/user_services_model.php */