<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Areae_model extends MY_Model {

	public $_table = 'areae';

	public function __construct()
	{
		parent::__construct();
	}

	public function parent_info($id)
	{
		$info = array();

		$select = 'id, area_name, parent_id, grandpa_id';
		$filter = array('id' => $id);
		$area = $this->get_one($select, $filter);

		if ( ! $area)
		{
			return NULL;
		}

		$data = array('grandpa_id' => $area->grandpa_id, 'parent_id' => $area->parent_id);

		foreach($data as $k => $v)
		{
			if ($v)
			{
				$select = 'id, area_name';
				$filter = array('id' => $v);
				$info[$k] = $this->get_one($select, $filter);
			}
		}

		$info['area_id'] = $area;

		return $info ;
	}

}

// END Areae_model class

/* End of file areae_model.php */
/* Location: ./application/model/areae_model.php */