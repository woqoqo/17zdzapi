<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Contacts_model extends MY_Model {

	public $_table = 'contacts';

	public function __construct()
	{
		parent::__construct();
	}

}

// END Contacts_model class

/* End of file contacts_model.php */
/* Location: ./application/model/contacts_model.php */