<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Messages_model extends MY_Model {

	public $_table = 'messages';

	public function __construct()
	{
		parent::__construct();
	}
}

// END Messages_model class

/* End of file messages_model.php */
/* Location: ./application/model/messages_model.php */