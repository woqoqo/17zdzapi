<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends MY_Model {

	public $_table = 'users';

	function __construct()
	{
		parent::__construct();
	}

}

// END Users_model class

/* End of file users_model.php */
/* Location: ./application/model/users_model.php */