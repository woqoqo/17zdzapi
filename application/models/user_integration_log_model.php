<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_integration_log_model extends MY_Model {

	public $_table = 'user_integration_log';

	function __construct()
	{
		parent::__construct();
	}

}

// END User_integration_log_model class

/* End of file user_integration_log_model.php */
/* Location: ./application/model/user_integration_log_model.php */