<?php

$lang['error_default_title'] = "提示";
$lang['error_email_or_password'] = "邮箱或密码不正确";
$lang['error_failure_account'] = "账号失效";
$lang['error_account_is_not_activated'] = "账号未激活";
$lang['error_account_is_not_exist'] = "账号不存在";
$lang['User does not exist'] = "用户名或密码错误";
$lang['Registration failed'] = "注册失败";
$lang['SMS verification code is not legally'] = "短信验证码不正确，请重新获取";
$lang['Phone already exists'] = "此手机号码已经注册过";
$lang['Verification code has been sent to your phone'] = "手机验证码已经发送到你的手机";