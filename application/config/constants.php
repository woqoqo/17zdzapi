<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/*
|--------------------------------------------------------------------------
| APP
|--------------------------------------------------------------------------
|
| These modes are used when working for APP
|
*/
define('APP_SECRET_KEY', 'cfba618d939bc0fe74929940f2e50f4f');
define('DEMO_ACCESS_TOKEN', '22d2afcf241c49d938251ea881d61080');
define('DEMO_CLIENT_ID', 'd2539e7f5ddb0f1c08bde069e2e8721b');

//friend
define('FRIEND_REQUEST_WAITING_PASS', 0);
define('FRIEND_REQUEST_CLOSE', -1);
define('FRIEND_REQUEST_PASS', 1);

//user status
define('USER_NOT_ACTIVE', 0);
define('USER_ACTIVE', 1);
define('USER_CLOSE', -1);

//user type
define('USER_TYPE_NORMAL', 1);
define('USER_TYPE_SERVICE', 2);



//question status
define('QUESTION_DEFAULT', 0);
define('QUESTION_ACCEPT', 1);
define('QUESTION_CANCEL', -1);


define('MESSAGE_UNREAD', 0);
define('MESSAGE_READ', 1);
define('MESSAGE_DELETED', -1);

// 1普通文本消息, 2图片, 3问题, 4产品, 5地理位置, 6语音
define('MESSAGE_TYPE_NORMAL', 1);
define('MESSAGE_TYPE_IMAGE', 2);
define('MESSAGE_TYPE_QUESTION', 3);
define('MESSAGE_TYPE_PRODUCT', 4);
define('MESSAGE_TYPE_LOCATION', 5);
define('MESSAGE_TYPE_RECORD', 6);

define('UNLIMIT', -1);
define('GROUP_USER_MAXIMUM', 7);

define('QUESTION_NOT_ADOPTE', 0);
define('QUESTION_ADOPTE', 1);

define('IMG_HOST', 'http://tripager-image.b0.upaiyun.com');
define('SOUND_HOST', 'http://data.17zdz.com');
define('AVATAR_HOST', 'http://tripager-avatar.b0.upaiyun.com');

define('DATA_TYEP_IMAGE', 1);
define('DATA_TYEP_SOUND', 2);

define('DATA_TYEP_MESSAGE_NAME', 'Message');
define('DATA_TYEP_SOUND_NAME', 'Sound');

define('IMAGE_SIZE_SUFFIX_100', '!L1.100');
define('IMAGE_SIZE_SUFFIX_300', '!L1.300');

/* End of file constants.php */
/* Location: ./application/config/constants.php */