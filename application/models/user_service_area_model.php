<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_service_area_model extends MY_Model {

	public $_table = 'user_service_area';

	function __construct()
	{
		parent::__construct();
	}

}

// END User_service_area_model class

/* End of file user_service_area_model.php */
/* Location: ./application/model/user_service_area_model.php */