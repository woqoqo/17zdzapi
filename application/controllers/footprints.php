<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Footprints extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Footprints_model');
	}

	public function new_list()
	{
		$select = 'id, uid, username, content, dateline, images, country_name, city_name,
		place_name, place_address, lng, lat, content, type_name, type_id, evaluate, dateline';
        
        $page = (int) $this->input->post('page', TRUE);

        if ($page <= 0) {
        	$page = 1;
        }

		$limit = '10';
		$offset = ($page - 1) * 20;

		$data = array()	;

		$filter = array();
		$place_name_tmp = $this->input->post('place_name', TRUE);
		$place_name = trim($place_name_tmp);
        
        $city_name_tmp = $this->input->post('city_name', TRUE);
		$city_name = trim($city_name_tmp);

		$latitude = (float) $this->input->post('latitude', TRUE);
		$longitude = (float) $this->input->post('longitude', TRUE);

		$type_id = $this->input->post('type_id');

		if(isset($_POST['evaluate'])) {
			$evaluate = (int) $this->input->post('evaluate');
			$filter['evaluate'] = $evaluate;
		}
		
		$uid = (int) $this->input->post('uid');

		if($uid > 0)
		{
			$filter['uid'] = $uid;

			$this->load->model('User_info_model');
			
			$userinfo = $this->User_info_model->get_one(
				'country_name, city_name, gender', 
				array(
					'uid' => $uid
				));

			$data['user_info'] = array();			
			$data['user_info']['place_info'] = $userinfo->country_name . $userinfo->city_name;
			$data['user_info']['gender'] = $this->User_info_model->get_gender_name_by_key($userinfo->gender);
		}

		if(! empty($place_name))
		{
			$this->db->like('place_name', $place_name);
		}
        
        if (! empty($city_name))
        {
            $this->db->like('city_name', $city_name);
        }

        if ($latitude) {
        	$squares = returnSquarePoint($longitude, $latitude);

        	$this->db->where('lat !=', 0);
        	$this->db->where('lat >', $squares['right-bottom']['lat']);
        	$this->db->where('lat <', $squares['left-top']['lat']);
        	$this->db->where('lng <', $squares['right-bottom']['lng']);
        	$this->db->where('lng >', $squares['left-top']['lng']);
        }

        if (is_array($type_id)) {
			foreach ($type_id as $v) {
				$this->db->or_like('type_id', $v);
			}
		}
        
		$this->Footprints_model->order('id DESC');
		$footprints = $this->Footprints_model->get_list($select, $filter, $limit, $offset);
   
		foreach($footprints as $k => $v) 
        {
            $images = array();
            $image_array = array();
            
            if ($v->images)
            {
                $image_array = unserialize($v->images);
                                
                foreach ($image_array as $url)
                {
                    $images[] = array(
                        'data_url' => $url . IMAGE_SIZE_SUFFIX_300,
                        'data_url_thumb' => $url . IMAGE_SIZE_SUFFIX_100
                    );
                }
            }
            
            $footprints[$k]->images = $images;
            $footprints[$k]->avatar = avatar_url($v->uid, '100');
            $footprints[$k]->normal_avatar = avatar_url($v->uid);
		}
        
		$data['timeline'] = $footprints;
        
        //log_var($data);
		
		show_ajax_success($data);
	}

	public function send()
	{
		$rule = 'required';
		$this->form_validation->set_rules('content', 'content', $rule);
		$this->form_validation->set_rules('place_address', 'placeAddress', $rule);
		$this->form_validation->set_rules('place_name', 'place_name', $rule);
		$this->form_validation->set_rules('latitude', 'latitude', $rule);
		$this->form_validation->set_rules('longitude', 'longitude', $rule);
		$this->form_validation->set_rules('country_name', 'country_name', $rule);
		$this->form_validation->set_rules('city_name', 'city_name', $rule);

		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '80001');
		}
		
		$data = array();
		$data['content'] = $this->input->post('content', TRUE);
		$data['place_address'] = $this->input->post('place_address', TRUE);
		$data['place_name'] = $this->input->post('place_name', TRUE);
		$data['lat'] = $this->input->post('latitude', TRUE);
		$data['lng'] = $this->input->post('longitude', TRUE);
		$data['country_name'] = $this->input->post('country_name', TRUE);
		$data['city_name'] = $this->input->post('city_name', TRUE);
		$data['dateline'] = $this->input->post('timestamp', TRUE);
		$data['evaluate'] = (int) $this->input->post('evaluate', TRUE);
		$data['uid'] = $this->uid;
		$data['username'] = $this->username;

		$type_name = $this->input->post('type_name', TRUE);

		if($type_name)
		{
			$data['type_name'] = $type_name;
		}

		$type_id = $this->input->post('type_id', TRUE);

		if($type_id)
		{
			$data['type_id'] = $type_id;
		}
        
        $images = $this->input->post('images', true);
        
        if (is_array($images) && count($images))
        {
            $data['images'] = serialize($images);
        }

		$this->Footprints_model->insert($data);

		show_ajax_success();
	}


	public function item()
	{
		$rule = 'required|is_natural_no_zero';
		$this->form_validation->set_rules('id', 'id', $rule);

		$id = $this->input->post('id', TRUE);

		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '80002');
		}


		$data = array();
		
		$select = '*';
		$filter = array(
			'id' => $id
		);

		$data['item'] = $this->Footprints_model->get_one($select, $filter);

		show_ajax_success($data);
	}

	public function places()
	{
		$rule = 'required|is_natural_no_zero';
		$this->form_validation->set_rules('uid', 'uid', $rule);

		$uid = $this->input->post('uid', TRUE);

		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '80002');
		}
        
		$data = array();
		
		$select = 'city_name';
		$filter = array(
			'uid' => $uid
		);

		$this->db->group_by('city_name');

		$data['places'] = $this->Footprints_model->get_list($select, $filter);
        
        //log_var($_POST);
        //log_message('debug', $this->db->last_query());


		show_ajax_success($data);
	}
}