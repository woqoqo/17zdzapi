<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common extends MY_Controller 
{
	public function continent_list() 
	{
		$data = array();

		$this->load->model('Areae_model');
		$filter = array('parent_id' => 0, 'grandpa_id' => 0);
		$this->Areae_model->order('recomment DESC');

		$data['areae'] = $this->Areae_model->get_list('id, area_name', $filter);
		
		show_ajax_success($data);
    }

    public function area_list()
    {
    	$parent_id = (int) $this->input->post('parent_id');

    	if($parent_id <= 0)
    	{
    		show_ajax_error('Area does not exist', '30001');
    	}

    	$this->load->model('Areae_model');
		$filter = array('parent_id' => $parent_id);
		$this->Areae_model->order('recomment DESC');

		$data['areae'] = $this->Areae_model->get_list('id, area_name', $filter);
        
        //array_unshift($data['areae'], array('id'=>0, 'area_name'=>'全部'));

		show_ajax_success($data);
    }
}
