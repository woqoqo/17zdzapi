<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sms_model extends MY_Model {

	public $_table = 'sms';

	function __construct()
	{
		parent::__construct();
	}

	public function send($phone_number, $verification_code, $message)
	{
		$result = '';

		$insert_data = array(
			'phone_number' => $phone_number,
			'verification_code' => $verification_code,
			'dateline' => dateline()
		);

		$sms_id = $this->insert($insert_data);

		if ($sms_id)
		{
			$this->load->library('sms');
			$this->sms->send($phone_number, $message);
			$result = $verification_code;
		}

		return $result;
	}
	
	public function verify($phone_number, $verification_code, $negate = FALSE)
	{
		$result = FALSE;
		$select = 'phone_number, verification_code, status, dateline';
		$filter = array(
			'phone_number' => $phone_number,
			'verification_code' => $verification_code,
			'status' => 1
		);

		$sms = $this->get_one($select, $filter);

		if ($sms)
		{
			if ($negate)
			{
				$update_data = array('status' => 0);
				$filter = array('verification_code' => $verification_code);

				$this->update($update_data, $filter);
			}

			$result = TRUE;
		}


		log_message('debug', '--------------------->>>>>>>>>');

		log_var($_POST);

		return $result;
	}

}

// END Sms_model class

/* End of file sms_model.php */
/* Location: ./application/model/sms_model.php */