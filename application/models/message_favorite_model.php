<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Message_favorite_model extends MY_Model {

    public $_table = 'message_favorite';

    public function __construct() {
        parent::__construct();
    }

}

// END Message_favorite_model class

/* End of file message_favorite_model.php */
/* Location: ./application/model/message_favorite_model.php */