<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Friends extends MY_Controller 
{
	public function __construct() {
		parent::__construct();
	}

	public function add_friend()
	{
		$this->form_validation->set_rules('uid', 'Uid', 'required|is_natural_no_zero');

		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '40001');
		}

		$uid = $this->input->post('uid');

		$select = 'id, username';
		$filter = array(
			'id' => $uid
		);
		$user = $this->Users_model->get_one($select, $filter);

		if (empty($user))
		{
			show_ajax_error('User does not exist', '40002');
		}

		$this->load->model('Friends_model');

		$insert_data = array(
			'uid' => $this->uid,
			'username' => $this->username,
			'friend_uid' => $uid,
			'friend_username' => $user->username,
			'dateline' => dateline()
		);
		$id = $this->Friends_model->insert($insert_data);

		$result = array();
		$result['id'] = $id;
		$result['title'] = 'friend/add_friend';

		show_ajax_success($result);
	}

	public function new_list()
	{
		$this->load->model('Friends_model');
		$select = 'uid, username, friend_uid, friend_username, dateline';
		$filter = array(
			'friend_uid' => $this->uid,
			'passed' => FRIEND_REQUEST_WAITING_PASS
		);


		$result = array();
		$result['friends'] = $this->Friends_model->get_list($select, $filter);
		$result['title'] = 'frineds/new_list';

		show_ajax_success($result);
	}

	public function clear_new_list()
	{
		$this->load->model('Friends_model');

		$filter = array(
			'passed' => FRIEND_REQUEST_WAITING_PASS,
			'friend_uid' => $this->uid
		);

		$update_data = array(
			'passed' => FRIEND_REQUEST_CLOSE
		);

		$this->Friends_model->update($update_data, $filter);

		$result = array();
		$result['title'] = 'frineds/clear_new_list';
		
		show_ajax_success($result);
	}

	public function adopt()
	{
		$this->form_validation->set_rules('id', 'Id', 'required|is_natural_no_zero');
		$this->form_validation->set_rules('adopt', 'Adopt', 'required|is_natural');


		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '40001');
		}

		$this->load->model('Friends_model');

		$filter = array(
			'passed' => FRIEND_REQUEST_WAITING_PASS,
			'friend_uid' => $this->uid
		);

		$update_data = array(
			'passed' => FRIEND_REQUEST_CLOSE
		);

		$this->Friends_model->update($update_data, $filter);

		$result = array();
		$result['title'] = 'frineds/adopt';

		show_ajax_success($result);
	}


}


// END Friends class

/* End of file friends.php */
/* Location: ./application/controllers/friends.php */