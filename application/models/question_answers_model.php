<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Question_answers_model extends MY_Model {

	public $_table = 'question_answers';

	function __construct()
	{
		parent::__construct();
	}
    
    public function status($uid)
    {
        $this->db->select('COUNT(*) AS count', FALSE);
        $this->db->group_by('status');
        $status_filter = array(
            'uid' => $uid
        );

        $status_info = $this->get_list('status', $status_filter);
        
        $status = array(
            'canceled' => 0,
            'default' => 0,
            'accepted' => 0,
            'adopted' => 0,
            'all' => 0
        );

        if ($status_info)
        {
            foreach ($status_info as $v)
            {
                switch ($v->status)
                {
                    case -1:
                        $status['canceled'] = $v->count;
                        break;

                    case 0:
                        $status['default'] = $v->count;
                        break;

                    case 1:
                        $status['accepted'] = $v->count;
                        break;

                    case 2:
                        $status['adopted'] = $v->count;
                        break;

                    default:
                        break;
                }

                $status['all'] += $v->count;
            }
        }
        
        return $status;
    }

}

// END Question_answers_model class

/* End of file question_answers_model.php */
/* Location: ./application/model/question_answers_model.php */