<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_info_model extends MY_Model {

	public $_table = 'user_info';

	function __construct()
	{
		parent::__construct();
	}

	public function get_gender_name_by_key($key)
    {
        $name = '未知';

        if($key == 1)
        {
            $name = '男';
        }
        else if ($key ==2)
        {
            $name = '女';
        }
        

        return $name;
    }	

}

// END User_info_model class

/* End of file user_info_model.php */
/* Location: ./application/model/user_info_model.php */