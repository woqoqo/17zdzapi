<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Groups_model extends MY_Model {

    public $_table = 'groups';

    public function __construct() {
        parent::__construct();
    }
}

// END Groups_model class

/* End of file groups_model.php */
/* Location: ./application/model/groups_model.php */