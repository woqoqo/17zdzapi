<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sign extends MY_Controller 
{
	function __construct()
	{
		parent::__construct();
	}

	/**
	 *  注册
	 * 
	 */
	public function up() 
	{   
		$insert_data = array();

		//form validation
		$this->_up_form_validation($insert_data);	

		//password
		$password = $this->input->post('password');
		$insert_data['password'] = hash_pwd($password);

		//register time
		$insert_data['registered'] = dateline();
       
		//insert user data
		$this->load->model('Users_model');

		$this->Users_model->insert($insert_data);
		$uid = $this->Users_model->insert_id();

		if( ! $uid) 
		{
			show_ajax_error('Registration failed', '20010');
		}

		//insert user auth data	
		$this->load->model('User_auth_model');
        
		$result = $auth_data = array(
			'uid' => $uid,
			'username' => $insert_data['username'],
			'access_token' => md5(randomkeys()),
			'client_id' => md5(randomkeys()),
			'dateline' => dateline()
		);

		$this->User_auth_model->insert($auth_data);

        $sign_type = (int) $this->input->post('sign_type');
        
		//insert user info data
		$this->load->model('User_info_model');
		$user_info = array(
			'uid' => $uid,
			'username' => $insert_data['username']
		);
        
        //send email
		if($sign_type === 1)
		{
            $user_info['gender'] = (int)$this->input->get_post('gender');
            $city_id =  (int)$this->input->get_post('city_id');
            
            if ($city_id) {
                $this->_deal_user_info_areae($city_id, $user_info);
            }
            
            $this->_up_send_email($insert_data);
        }

        $this->User_info_model->insert($user_info);

		//return result
        $result['phone'] = $insert_data['phone'];
        $result['email'] = $insert_data['email'];
        
		show_ajax_success($result);
	}
    
    private function _deal_user_info_areae($city_id, &$user_info) {
        $this->load->model('Areae_model');
        $areae = $this->Areae_model->parent_info($city_id);

        if ($areae) {
            if (isset($areae['grandpa_id'])) {
                $user_info['continent_id'] = $areae['grandpa_id']->id;
                $user_info['continent_name'] = $areae['grandpa_id']->area_name;
            }

            if (isset($areae['parent_id'])) {
                $user_info['country_id'] = $areae['parent_id']->id;
                $user_info['country_name'] = $areae['parent_id']->area_name;
            }

            $user_info['city_id'] = $areae['area_id']->id;
            $user_info['city_name'] = $areae['area_id']->area_name;
            $user_info['place_info'] = 
                    $user_info['continent_name'] . ' ' . 
                    $user_info['country_name'] . ' ' . 
                    $user_info['city_name'];
        }
    }

    /**
	 * 获取短信验证码
	 * 
	 */

	public function up_send_sms()
	{
		$rule = 'required|exact_length[11]|is_natural_no_zero';
		$this->form_validation->set_rules('phone', 'Phone', $rule);
		$phone = $this->input->post('phone');

		if ($this->form_validation->run() === FALSE) {
            show_ajax_error(validation_errors(), '20005');
        }

        $verification_code = randomkeys(6, TRUE);
		$data = array('verification_code' => $verification_code);
		$message = $this->load->view('sms/sign/up', $data, TRUE);

		$this->load->model('Sms_model');
		$result = $this->Sms_model->send($phone, $verification_code, $message);

		show_ajax_result($result);
	}

	private function _up_send_email($user)
	{
		$subject = '欢迎使用一起找地主';
		$message = $this->load->view('email/sign/up', $user, TRUE);

		$this->load->library('email');
		$this->email->from('service@17zdz.com', '17zdz');
		$this->email->to($user['email']);
		$this->email->subject($subject);
		$this->email->message($message);
		$this->email->send();
        
        error_log($this->email->print_debugger());
	}

	private function _up_form_validation(& $insert_data)
	{
		//log_message('debug', serialize($_POST));

		$sign_type = (int) $this->input->post('sign_type');

		$username = $this->input->post('username');
		//$this->form_validation->set_rules('username', 'Username', 'required|min_length[3]');
		$insert_data['username'] = $username;

		if($sign_type === 1)
		{
			$rule = 'required|valid_email|is_unique[users.email]';
			$this->form_validation->set_rules('email', 'Email', $rule);
			$email = $this->input->post('email');
			$length = stripos($email, '@');

			//email && username
			$insert_data['email'] = $email;
			$insert_data['phone'] = '';
        	$insert_data['username'] = substr($email, 0, $length);
        	$insert_data['activation'] = md5(randomkeys());
		}
		else
		{
        	$rule = 'required|exact_length[11]|is_natural_no_zero|is_unique[users.phone]';
			$this->form_validation->set_rules('phone', 'Phone', $rule);
			$phone = $this->input->post('phone');
			
			//phone && username
			$insert_data['phone'] = $phone;
			$insert_data['email'] = '';

			//sms_verification_code
			$this->form_validation->set_rules('sms_code', 'SMS verification code', 'required|exact_length[6]');

			$verification_code = $this->input->post('sms_code');

			$this->load->model('Sms_model');
			$result = $this->Sms_model->verify($phone, $verification_code, TRUE);

			if($result === FALSE)
			{
				show_ajax_error('SMS verification code is not legally', '20004');
			}
		}
        
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');		

		if ($this->form_validation->run() === FALSE)
		{               
			show_ajax_error(validation_errors(), '20001');
		}
	}

	/**
	 * 登录
	 * 
	 */

	public function in() {
		$user = $this->_check_user();

		$this->load->model('User_auth_model');
        $this->load->model('User_info_model');

		$result = $auth_data = array(
			'uid' => $user->id,
			'username' => $user->username,
			'access_token' =>  md5(randomkeys()),
			'client_id' => md5(randomkeys()),
			'dateline' => dateline()
		);

		$this->User_auth_model->insert($auth_data);
        
		$user_select = 'nickname, gender, idle, question_accepted, question_answered, 
        avatar, continent_id, continent_name, country_id, country_name, city_id, 
        city_name, place_info';        
		$user_info = $this->User_info_model->get_one($user_select, array( 'uid' => $user->id));
        
		$data = array_merge($result, (array) $user_info);
		$data['email'] = $user->email;
		$data['phone'] = $user->phone;

		show_ajax_success($data);
	}
    
    private function _check_user()
    {
        $filter = array();
		$this->_in_form_validation($filter);

		$password = $this->input->post('password');
		$filter['password'] = hash_pwd($password);

		$this->load->model('Users_model');
		$user = $this->Users_model->get_one('id, username, status, phone, email', $filter);

		if ( ! $user) { show_ajax_error('User does not exist', '21010'); }
		if ($user->status == 0) { /*show_ajax_error('Account is not activated', '21011');*/ }
        
        return $user;
    }

	private function _in_form_validation(& $filter)
	{
		$sign_type = $this->input->post('sign_type');

		if($sign_type == 1)
		{
			$rule = 'required|valid_email';
			$this->form_validation->set_rules('email', 'Email', $rule);
			$filter['email'] = $this->input->post('email');
		}
		else
		{
			$rule = 'required|exact_length[11]|is_natural_no_zero';
			$this->form_validation->set_rules('phone', 'Phone', $rule);
			$filter['phone'] = $this->input->post('phone');
		}

		$this->form_validation->set_rules('password', 'Password', 'required|min_length[5]');

		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '22001');
		}
	}

	
	/**
	 * 退出
	 */
	public function out()
	{
		$this->load->model('User_auth_model');
		
		$filter = array(
			'client_id' => $this->client_id
		);

		$result = $this->User_auth_model->delete($filter);

		show_ajax_result($result);
	}

	public function forgot_password()
	{
		$email = $this->input->post('email');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');

		if ($this->form_validation->run() === FALSE)
		{
            //error_log(validation_errors());
			show_ajax_error(validation_errors(), '23001');
		}

		$filter = array(
			'email' =>  $email
		);

		$this->load->model('Users_model');

		$count = $this->Users_model->count_all($filter);
        
		if($count == 0)
		{
			show_ajax_error('User does not exist', '23002');
		}

		$password = randomkeys(6, TRUE);
		
		$update_data = array(
			'password' => hash_pwd($password)
		);

		$this->Users_model->update($update_data, $filter);
        
        
        //send mail
		$subject = '一起找地主：找回密码';
        $data = array('password' => $password);
		$message = $this->load->view('email/sign/forgot_password', $data, TRUE);
        
		$this->load->library('email');
		$this->email->from('service@17zdz.com', '17zdz');
		$this->email->to($email); 
		$this->email->subject($subject);
		$this->email->message($message); 
		$this->email->send();
        
        //error_log($this->email->print_debugger());
         
		show_ajax_success("Password has been sent to your email");
	}
    
//    public function forgot_password()
//	{
//		$rule = 'required|exact_length[11]|is_natural_no_zero';
//		$this->form_validation->set_rules('phone', 'Phone', $rule);
//		$phone = $this->input->post('phone');
//
//		$username = $this->input->post('username');
//		$this->form_validation->set_rules('username', 'Nickname verification code', 'required|min_length[3]');
//
//		if ($this->form_validation->run() === FALSE)
//		{
//			show_ajax_error(validation_errors(), '23001');
//		}
//
//		$filter = array(
//			'username' =>  $username,
//			'phone' => $phone
//		);
//
//		$this->load->model('Users_model');
//
//		$count = $this->Users_model->count_all($filter);
//
//		if($count == 0)
//		{
//			show_ajax_error('User does not exist', '23002');
//		}
//
//		$password = randomkeys(6, TRUE);
//		
//		$update_data = array(
//			'password' => hash_pwd($password)
//		);
//
//		$this->Users_model->update($update_data, $filter);
//
//		$data = array('password' => $password);
//		$message = $this->load->view('sms/sign/forgot_password', $data, TRUE);
//		
//		$this->load->library('sms');
//		$this->sms->send($phone, $message);
//
//		show_ajax_success("Password has been sent to your phone");
//	}

}


// END Sign class

/* End of file sign.php */
/* Location: ./application/controllers/sign.php */