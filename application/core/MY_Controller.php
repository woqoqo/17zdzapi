<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $controller   = '';
    public $action       = '' ;
    public $client_id = '';
    public $uid = 0;
    public $page = 1;
    public $username = '';
    public $localization = 'en';

    public function __construct()
    {
        parent::__construct();

        $this->controller = $this->router->fetch_class();
        $this->action     = $this->router->fetch_method();
        $url_path         = $this->controller . '/' . $this->action  ;

        $not_validate = array(
            'sign/up', 'sign/in', 'sign/up_send_sms', 'sign/forgot_password',
            'common/continent_list', 'common/area_list'
        );

        if(in_array($url_path, $not_validate)) 
        {
            $validate_access_token = FALSE;
        } 
        else 
        {
            $validate_access_token = TRUE;
        }

        if($this->controller != 'home')
        {
            $this->_safe_validation($validate_access_token);
        }
        
        $this->localization = $this->input->get_post('localization', TRUE);
        
        if ($this->localization != 'zh_Hans')
        {
            $this->localization =  'en';
        }
    }

	/**
	 *  安全验证
	 *
	 */

    protected function _safe_validation($validate_access_token = TRUE) {
        $sign = $this->input->get_post('sign');
        $access_token = $this->input->get_post('access_token');
        $timestamp = (int) $this->input->get_post('timestamp');

        if (empty($sign) || strlen($sign) != 32) {
            show_ajax_error('Sign is not legitimate', '00001');
        }

        if ($validate_access_token === TRUE) {
            if (empty($access_token) || strlen($access_token) != 32) {
                show_ajax_error('Access token is not legitimate', '00002');
            }

            $this->load->model('User_auth_model');

            $select = 'uid, username, client_id, access_token';
            $filter = array(
                'access_token' => $access_token
            );

            $user = $this->User_auth_model->get_one($select, $filter);

            if ($user) {
                if ($user->access_token != $access_token) {
                    show_ajax_error('Access token is not legitimate', '00002');
                }

                //user info
                $this->client_id = $user->client_id;
                $this->uid = $user->uid;
                $this->username = $user->username;
            } else {
                show_ajax_error('Access token is not legitimate', '00002');
            }

            if ($timestamp <= 0) {
                show_ajax_error('Time out', '00003');
            }


            $now = $this->input->server('REQUEST_TIME');
            $time_difference = $now - $timestamp;

            if ($time_difference > 3000) {
                show_ajax_error('Time out', '00004');
            }

            $required = array(
                'sign' => '',
                'access_token' => '',
                'timestamp' => '',
                'client' => '',
                'c' => '',
                'm' => '',
                'd' => ''
            );

            $param_data = $this->input->server('REQUEST_METHOD') == 'GET' ? $_GET : $_POST;
            $url_param = array_diff_key($param_data, $required);

            if (!empty($url_param)) {
                ksort($url_param);
            }

            $url_param_str = '';

            if (!empty($url_param)) {
                foreach ($url_param as $k => $v) {
                    if (is_array($v)) {
                        continue;
                    }

                    $url_param_str .= $k . $v;
                }
            }

            $md5_str = $access_token . $url_param_str . $timestamp . APP_SECRET_KEY . $this->client_id;
            $_sign = md5($md5_str);

            if ($_sign != $sign) {
                show_ajax_error('Signature illegal', '00006');
            }
        }
    }

    protected function view($view, $data = array())
    {
        $this->load->view('header', $data);
        $this->load->view($view, $data);
        $this->load->view('footer', $data);
    }

    protected function set_page($count_all, $limit = 20) {
        if ($count_all > $limit) {

            $r = $count_all / $limit;

            if ($r > 1) {
                $this->page_count = ceil($r);
            }
        }

        $this->page = (int) $this->input->get('p');

        if ($this->page <= 0) {
            $this->page = 1;
        }

        return ($this->page - 1) * $limit;
    }
}

// END MY_Controller class

/* End of file MY_Controller.php */
/* Location: ./application/core/MY_Controller.php */