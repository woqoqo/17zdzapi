<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Message_items_model extends MY_Model {

    public $_table = 'message_items';

    public function __construct() {
        parent::__construct();
    }

    public function message_flag($uid, $other_uid) {
        $message_flag = $uid > $other_uid ? $other_uid . '-' . $uid : $uid . '-' . $other_uid;

        return substr(md5($message_flag), 0, 6);
    }

    public function insert_message($data) {        
        $dateline = dateline();
        
        $data_type = $data['data_type'];
        
        if ($data_type <= 0) {
            $data_type = 1;
        }
        
        $data_source =  isset($data['data_source']) ? $data['data_source'] : ' ';
        
        $group_id =  0;
        
        if (isset($data['group_id'])) {
            $group_id = (int) $data['group_id'];
        }
        
        if ($group_id > 0) {
            $flag = substr(md5($data['group_id']), 0, 6);
        }
        else {
            $flag = $this->message_flag($data['other_uid'], $data['uid']);
        }
        
        $this->db->query('SET NAMES utf8mb4');
        
        $my_message = array(
            'flag' => $flag,
            'content' => $data['content'],
            'uid' => $data['uid'],
            'username' => $data['username'],
            'other_uid' => $data['other_uid'],
            'other_username' => $data['other_username'],
            'sender_uid' => $data['uid'],
            'dateline' => $dateline,
            'filename' => $data['filename'],
            'duration' => $data['duration'],
            'image_width' => $data['image_width'],
            'image_height' => $data['image_height'],
            'data_type' => $data_type,
            'data_id' => $data['data_id'],
            'data_source' => $data_source,
            'group_id' => $group_id,
            'status' => $data['status']
        );
        
        $insert_data = array();
        $insert_data[] = $my_message;
        
        if ($group_id > 0) 
        {            
            //群组成员
            $this->load->model('Group_users_model');
            $group_user_select = 'uid, username';
            $group_user_filter = array(
                'group_id' => $group_id
            );

            $group_users = $this->Group_users_model->get_list($group_user_select, $group_user_filter);
            if (! $group_users) {
                show_ajax_error('Group does not exist', '100013');
            }
                        
            $group_user_id = array();
            foreach($group_users as $v)
            {
                $group_user_id[] = $v->uid;
            }

            if ( !in_array($this->uid, $group_user_id)) {
                show_ajax_error('Group does not exist', '100014');
            }
            
            
            foreach ($group_users as $v)
            {
                if ($v->uid == $this->uid) {
                    continue;
                }
                
                $insert_data[] = array(
                    'flag' => $flag,
                    'content' => $data['content'],
                    'uid' => $v->uid,
                    'username' => $v->username,
                    'other_uid' => $data['uid'],
                    'other_username' => $data['username'],
                    'sender_uid' => $data['uid'],
                    'dateline' => $dateline,
                    'filename' => $data['filename'],
                    'duration' => $data['duration'],
                    'image_width' => $data['image_width'],
                    'image_height' => $data['image_height'],
                    'data_type' => $data_type,
                    'data_id' => $data['data_id'],
                    'data_source' => $data_source,
                    'group_id' => $group_id,
                    'status' => MESSAGE_UNREAD
                );
            }
            
        }
        else {
            $insert_data[] = array(
                'flag' => $flag,
                'content' => $data['content'],
                'uid' => $data['other_uid'],
                'username' => $data['other_username'],
                'other_uid' => $data['uid'],
                'other_username' => $data['username'],
                'sender_uid' => $data['uid'],
                'dateline' => $dateline,
                'filename' => $data['filename'],
                'duration' => $data['duration'],
                'image_width' => $data['image_width'],
                'image_height' => $data['image_height'],
                'data_type' => $data_type,
                'data_id' => $data['data_id'],
                'data_source' => $data_source,
                'group_id' => $group_id,
                'status' => MESSAGE_UNREAD
            );
        }
        
        $this->insert_batch($insert_data);
        
        $my_message['id'] = $this->insert_id();
               
        
            
        log_message('error', '-----> $my_message');
        log_var($my_message);
        
        
        return $my_message;
    }
}

// END Message_items_model class

/* End of file message_items_model.php */
/* Location: ./application/model/message_items_model.php */