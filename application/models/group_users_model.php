<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Group_users_model extends MY_Model {

    public $_table = 'group_users';

    public function __construct() {
        parent::__construct();
    }
}

// END Group_users_model class

/* End of file group_users_model.php */
/* Location: ./application/model/group_users_model.php */