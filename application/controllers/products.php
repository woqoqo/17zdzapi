<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products extends MY_Controller 
{
	public function __construct()
	{
		parent::__construct();
	}
    
    public function consult() {
        
        $id = (int) $this->input->get_post('id');
        
        if ($id <= 0) {
            show_ajax_error('Product does not exist', '90002');
        }
        
        //产品信息
        //$product;
        
        $this->load->model('Message_items_model');
        
        //插入消息
        $data = array();

        //接收者 uid
        $data['other_uid'] = 27; //$product->uid;

        //接收者 username
        $data['other_username'] = '途牛'; //$product->username;

        //产品咨询者 
        $data['uid'] = $this->uid;

        //产品咨询者  username 
        $data['username'] = $this->username;

        //消息内容
        $data['content'] = 'product';

        //消息类型： 1普通消息, 2群组, 3问题, 4产品
        $data['data_type'] = MESSAGE_TYPE_PRODUCT;
        
        //产品ID, 暂时用1
        $data['data_id'] = 1; //$product->id;

        //关联文件的名称
        $data['filename'] = (string) $this->input->post('filename');
        
        //录音的名称, 长度
        $data['duration'] = 0;
        
        //图片的高度,宽度
        $data['image_width'] = (float) $this->input->post('image_width');
        $data['image_height'] = (float) $this->input->post('image_height');
        
        //消息的状态
        $data['status'] = MESSAGE_UNREAD;
        
        
        //关联的产品内容
        $tmp_content = '1、交通：上海、北京至香港往返机票/深圳、广州至香港往返汽车票（双人）。
2、团期：4-5月团期. 会优先按照您的时间安排出游计划（机票/汽车票、酒店、当地游），如遇资源紧张或其他客观因素，途牛保留更换至其他日期出游的权利；
3、行程：3晚4日自助游行程，特别赠送米其林推荐的西贡海鲜餐1日游2份。
4、酒店：3晚三星级酒店标准间（双床）。';
        
         $data_source = array(
            'id' => '1',
            'uid' => '27',
            'username' => '牛牛',
            'title' => '香港米其林美食4日双飞自助游',
            'places' => '香港',
            'days' => 10,
            'cost' => 1008,
            'content' => mb_substr($tmp_content, 0, 50),
            'dateline' => '1395374142',
            'cover' => 'http://img.17zdz.com/areae/bkssh6irv3t9905xleto7urp3bohsz64.jpg'
        );
         
         $data['data_source'] = json_encode($data_source);

        //引入 Message_items_model
        $this->load->model('Message_items_model');
        
        //插入数据
        $this->Message_items_model->insert_message($data);
        
        show_ajax_success();
    }
    
   
	public function recommont() {
        $products = array();
        $products[] = array(
            'id' => 1,
            'uid' => '27',
            'username' => '牛牛',
            'title' => '香港美食4日双飞自助游',
            'places' => '双人套餐，全国出发，特别赠送',
            'days' => 10,
            'cost' => 1888,
            'avatar' => 'http://tripager-avatar.b0.upaiyun.com/tuniu.jpg!square.100'
        );
        $products[] = array(
            'id' => 2,
            'uid' => '28',
            'username' => '携程',
            'title' => '希腊5晚8天游：减500',
            'places' => '米岛圣岛，雅典观光，情定爱情海',
            'days' => 18,
            'cost' => 2888,
            'avatar' => 'http://tripager-avatar.b0.upaiyun.com/xiechen.jpg!square.100'
        );

        $products[] = array(
            'id' => 3,
            'uid' => '29',
            'username' => '淘宝',
            'title' => '三亚双飞5日游:2人减600',
            'places' => '3晚180度海景，1晚宿蜈支洲',
            'days' => 4,
            'cost' => 888,
            'avatar' => 'http://tripager-avatar.b0.upaiyun.com/taobao.jpg!square.100'
        );


        $products[] = array(
            'id' => 4,
            'uid' => '30',
            'username' => '去哪儿',
            'title' => '莫斯科-圣堡9日游',
            'places' => '东航直飞，冬宫，金环三镇，深度游览',
            'days' => 9,
            'cost' => 2666,
            'avatar' => 'http://tripager-avatar.b0.upaiyun.com/qunaer.jpg!square.100'
        );

        $data = array();
        $data['products'] = $products;

        show_ajax_success($data);
    }

    public function items() {
        $id = (int) $this->input->get_post('id');

        if ($id <= 0) {
            show_ajax_error('Product does not exist', '90001');
        }

        $products = array();
        $products[] = array(
            'id' => '1',
            'uid' => '27',
            'username' => '牛牛',
            'title' => '香港米其林美食4日双飞自助游',
            'places' => '双人套餐，全国出发，特别赠送',
            'days' => 10,
            'cost' => 1008,
            'avatar' => 'http://tripager-avatar.b0.upaiyun.com/dc58e3a306451c9d670adcd37004f48f.jpg!square.100',
            'content' => '1、交通：上海、北京至香港往返机票/深圳、广州至香港往返汽车票（双人）。
2、团期：4-5月团期. 会优先按照您的时间安排出游计划（机票/汽车票、酒店、当地游），如遇资源紧张或其他客观因素，途牛保留更换至其他日期出游的权利；
3、行程：3晚4日自助游行程，特别赠送米其林推荐的西贡海鲜餐1日游2份。
4、酒店：3晚三星级酒店标准间（双床）。
',
            'dateline' => '1395374142',
            'cover' => 'http://img.17zdz.com/areae/bkssh6irv3t9905xleto7urp3bohsz64.jpg!L1.300'
        );
       $products[] = array(
            'id' => '14',
            'uid' => '27',
            'username' => '牛牛',
            'title' => '三亚双飞5日游:2人减600',
            'places' => '双人套餐，全国出发，特别赠送',
            'days' => 10,
            'cost' => 1008,
            'avatar' => 'http://tripager-avatar.b0.upaiyun.com/dc58e3a306451c9d670adcd37004f48f.jpg!square.100',
            'content' => '1、交通：上海、北京至香港往返机票/深圳、广州至香港往返汽车票（双人）。
2、团期：4-5月团期. 会优先按照您的时间安排出游计划（机票/汽车票、酒店、当地游），如遇资源紧张或其他客观因素，途牛保留更换至其他日期出游的权利；
3、行程：3晚4日自助游行程，特别赠送米其林推荐的西贡海鲜餐1日游2份。
4、酒店：3晚三星级酒店标准间（双床）。
',
            'dateline' => '1395374142',
            'cover' => 'http://img.17zdz.com/areae/bkssh6irv3t9905xleto7urp3bohsz64.jpg!L1.300'
        );

        $data = array();
        $data['products'] = $products;

        show_ajax_success($data);
    }

    public function info() {
        $id = (int) $this->input->get_post('id');

        if ($id <= 0) {
            show_ajax_error('Product does not exist', '90001');
        }

        $product = array(
            'id' => '1',
            'uid' => '27',
            'username' => '牛牛',
            'title' => '香港米其林美食4日双飞自助游',
            'places' => '香港',
            'days' => 10,
            'cost' => 1888,
            'avatar' => 'http://tripager-avatar.b0.upaiyun.com/dc58e3a306451c9d670adcd37004f48f.jpg!square.100',
            'content' => '1、交通：上海、北京至香港往返机票/深圳、广州至香港往返汽车票（双人）。

2、团期：4-5月团期. 会优先按照您的时间安排出游计划（机票/汽车票、酒店、当地游），如遇资源紧张或其他客观因素，途牛保留更换至其他日期出游的权利；

3、行程：3晚4日自助游行程，特别赠送米其林推荐的西贡海鲜餐1日游2份。

4、酒店：3晚三星级酒店标准间（双床）。

费用包含

1.交通：往返团队经济舱机票含税费（团队机票将统一出票，如遇政府或航空公司政策性调整燃油税费，在未出票的情况下将进行多退少补，敬请谅解。团队机票一经开出，不得更改、不得签转、不得退票），当地旅游巴士。

2.签证：团队旅游签证800元/人。

3.住宿：行程所列酒店。

4.用餐：行程中团队标准用餐，（中式餐或自助餐或特色餐，含飞机上用餐，自由活动期间用餐请自理；如因自身原因放弃用餐，则餐费不退）。

5.门票：行程中所含的景点首道大门票。

6.导服：专职中文领队兼导游。

7.儿童价标准：年龄2~8周岁（不含），不占床。

费用不包含

1.单房差。

2.其他：预定城市与出发城市之间往返交通费用。

3.出入境个人物品海关征税，超重行李的托运费、保管费。

4.因交通延阻、战争、政变、罢工、天气、飞机机器故障、航班取消或更改时间等不可抗力原因所引致的额外费用。

5.酒店内洗衣、理发、电话、传真、收费电视、饮品、烟酒等个人消费。

6.当地参加的自费以及以上“费用包含”中不包含的其它项目。

* 建议购买旅游人身意外保险 永诚保险境外旅游意外保险 美亚保险境外旅游意外保险 都邦境外旅游意外保险 平安境外旅游意外保险 ',
            'dateline' => '1395374142',
            'cover' => 'http://tripager-avatar.b0.upaiyun.com/ad.jpg'
        );

        $data = array();
        $data['product'] = $product;

        show_ajax_success($data);
    }

}