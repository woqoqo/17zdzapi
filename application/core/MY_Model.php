<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Model extends CI_Model {
	protected $_table = '';
    private $_result_type = 'object';

    public function __construct()
    {
        parent::__construct();
    }

    public function query($sql)
    {
        $this->db->query($sql); 
    }
    
    public function set_result_type($result_type) {
        $this->_result_type = $result_type;
    }

    public function insert($data) {
    	$this->db->insert($this->_table, $data);

        return $this->db->insert_id();
    }

    public function insert_batch($data) {            
        return $this->db->insert_batch($this->_table, $data);
    }

    public function insert_id() {
    	return $this->db->insert_id();
    }

    public function update($data, $filter)
    {
        $this->db->where($filter);
        $this->db->update($this->_table, $data);

        return $this->db->affected_rows();
    }

    private function _row(& $query) {
        if ($this->_result_type == 'object') {
            $result = $query->row();
        } else {
            $result = $query->row_array();
        }

        return $result;
    }

    /**
     * 获取一条数据
     * 
     * @param string $select
     * @param mixed $filters 
     * @return mixed 
     */

    public function get_one($select = '', $filters = array()) {
        $limit = 1;

        if ($select) {
            $this->db->select($select);
        }

        if ($filters) {
            $query = $this->db->get_where($this->_table, $filters, $limit);
        } else {
            $query = $this->db->get($this->_table, $limit);
        }

        return $this->_row($query);
    }

    private function _result(& $query) {
        if ($this->_result_type == 'object') {
            $result = $query->result();
        } else {
            $result = $query->result_array();
        }

        return $result;
    }

    /**
     * 获取列表数据
     * 
     * @param string $select
     * @param mixed $filters
     * @param int $limit
     * @param int $offset
     * @return mixed 
     */
    public function get_list($select = '', $filters = array(), $limit = 500, $offset = 0) {
        if ($select) {
            $this->db->select($select);
        }

        if ($filters) {
            $this->db->where($filters);
        }

        if ($limit == UNLIMIT) {
            $query = $this->db->get($this->_table);
        } else {
            $query = $this->db->get($this->_table, $limit, $offset);
        }
        
        return $this->_result($query);
    }

    public function count_all($filter = '') {
        $query = $this->get_one('COUNT(*) AS count', $filter);

        return $query->count;
    }

    public function order($order) {
        $this->db->order_by($order);
    }

    /**
     * 删除数据
     * 
     * @param mixed $filters
     * @return int  
     */
    public function delete($filter)
    {
        $this->db->where($filter);
        $this->db->delete($this->_table);

        return $this->db->affected_rows();
    }

    public function select_sum($select, $as)
    {
        $this->db->select_sum($select, $as);
    }

    public function group_by($group)
    {
        $this->db->group_by($group);
    }
}

// END MY_Model class

/* End of file MY_Model.php */
/* Location: ./application/core/MY_Model.php */