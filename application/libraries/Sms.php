<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

class Sms {

    public function send($phone, $msg) {
        $url = 'http://ws.9orange.net:8080/NOSmsPlatform/server/SMServer.htm';

        $para = http_build_query(array(
            'types' => 'send',
            'account' => 'anzheng',
            'password' => '123456',
            'destmobile' => $phone,
            'msgText' => $msg,
        ));

        $responseText =  $this->post_request($url, $para);
        
        return $responseText;
    }

    function post_request($url, $para) {


        $curl = curl_init($url);
        
        curl_setopt($curl, CURLOPT_HEADER, 0); // 过滤HTTP头
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); // 显示输出结果
        curl_setopt($curl, CURLOPT_POST, true); // post传输数据
        curl_setopt($curl, CURLOPT_POSTFIELDS, $para); // post传输数据
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 30);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);

        $responseText = curl_exec($curl);
        $info = curl_getinfo($curl);

        log_var($responseText);
        log_var($info);

        //print_r($responseText);
        
        curl_close($curl);

        return $responseText;
    }

}

// END Sms class

/* End of file Sms.php */
/* Location: ./application/libraries/Sms.php */