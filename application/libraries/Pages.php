<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Pages {

    public $params = array();
    public $page = 1;
    public $page_count = 0;

    public function init_page($count_all, $limit = 20) {
        if ($count_all > $limit) {

            $r = $count_all / $limit;

            if ($r > 1) {
                $this->page_count = ceil($r);
            }
        }

        $CI =& get_instance();

        $this->page = (int) $CI->input->get('p');

        if ($this->page <= 0) {
            $this->page = 1;
        }

        return ($this->page - 1) * $limit;
    }

    public function url_param($url_param = array()) {
        $this->params = $url_param;
    }

    public function pagination() {
        $url_param_str = '?';

        $i = 0;

        if (empty($this->params) === FALSE) {
            foreach ($this->params as $k => $v) {
                $url_param_str .= $i == 0 ? "$k=$v" : "&$k=$v";
                $i++;
            }

            $url_param_str .= '&p=';
        } else {
            $url_param_str .= 'p=';
        }

        $pagination = '';

        if ($this->page_count > 1) {

            if (($this->page - 5) <= 0) {
                $i = 1;
                $end = 10;

                if ($end > $this->page_count) {
                    $end = $this->page_count;
                }
            } else {
                $end = $this->page + 4;

                if ($end > $this->page_count) {
                    $end = $this->page_count;
                }

                $i = $end - 9;

                if ($i <= 0) {
                    $i = 1;
                }
            }

            $pagination = '<div class="pagination"><ul>';

            if ($this->page > 1) {
                $pagination .= '<li><a href="' . $url_param_str . '1">第一页</a></li>';
                $pagination .= '<li><a href="' . $url_param_str . ($this->page - 1) . '">&laquo;</a></li>';
            }

            for ($i; $i <= $end; $i++) {
                if ($i == $this->page) {
                    $pagination .= '<li class="active"><span>' . $i . '</span></li>';
                } else {
                    $pagination .= '<li><a href="' . $url_param_str . $i . '">' . $i . '</a></li>';
                }
            }

            if (($this->page + 1) <= $this->page_count) {
                $pagination .= '<li><a href="' . $url_param_str . ($this->page + 1) . '">»</a></li>';
                $pagination .= '<li><a href="' . $url_param_str . $this->page_count . '">最后一页</a></li>';
            }

            $pagination .= '</ul></div>';
        }

        return $pagination;
    }   
}