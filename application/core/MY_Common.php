<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function show_ajax_result($result)
{
    $result ? show_ajax_success() : show_ajax_error();
}

function _deal_msg($data)
{
    $result = array();

    if (is_string($data)) {
        $result['msg'] = $data;
    } 
    elseif (is_array($data)) 
    {
        $result = $data;
    }
    
    $CI =& get_instance();

    if (isset($result['msg']) && !empty($result['msg']) && $CI->localization == 'zh_Hans')
    {
        $CI->lang->load('error', 'zh_cn');
                
        $msg = $CI->lang->line($result['msg']);
        
        if ($msg)
        {            
            $result['msg'] = $CI->lang->line($result['msg']);
        }
    }

    return $result;
}

function show_ajax_success($data = 'Done') 
{
    $result = _deal_msg($data);
    
    $result['error'] = 0;

    echo json_encode($result);

    exit();
}

/*
 * ajax 错误信息
 * 
 * @param mixed $data
 * @param int $error 1:逻辑错误, 2,未登陆, 3,验证码
 * @return string
 */

function show_ajax_error($data = 'Error', $code='00000', $error = 1) 
{
    $result = _deal_msg($data);
    $result['code'] = $code;
    $result['error'] = $error;

    echo json_encode($result);

    exit();
}

/**
 * 生成随机字符串
 * 
 * @param int $length 字符串的长度
 * @param bool $number 是否返回为数字的字符串
 * @return string 字符串
 */
function randomkeys($length = 20, $number = FALSE) 
{
    $pattern = $number ? '1234567890' : '1234567890abcdefghijklmnopqrstuvwxyz';
    $key = '';
    for ($i = 0; $i < $length; $i++) {
        $key .= $pattern{mt_rand(0, strlen($pattern) - 1)};
    }

    return $key;
}

function fetch_user_data_path($filename = "")
{
    if (empty($filename)) {
        return '';
    }
    
    $type = pathinfo($filename, PATHINFO_EXTENSION);
    
    $path_name = md5(substr(md5($filename), 0, 6));

    $host = IMG_HOST;
    
    if ($type == 'caf')
    {
        $host = SOUND_HOST;
    }
    
    return $host. '/' . $path_name . '/' . $filename ;
}

/**
 * 生成hash
 * 
 * @param string $pwd 原始加密字符
 * @return string 加密后字符串
 */
function hash_pwd($pwd) 
{
    $pwd = substr(md5($pwd), 5, 20);
    $pwd = substr(sha1($pwd), 10, 20);

    return $pwd;
}


function dateline($time = '')
{
    /*
    if(! $time)
    {
        $time =  time();
    }
    
    return date('Y-m-d H:i:s', $time);
    */
    
    return $_SERVER['REQUEST_TIME'];
}


function service_type_data()
{
    return array(
            array('name' => '交通', 'id' => 1 ),
            array('name' => '美食', 'id' => 2 ),
            array('name' => '购物', 'id' => 3 ),
            array('name' => '住宿', 'id' => 4 ),
            array('name' => '景点', 'id' => 5 ),
            array('name' => '其他', 'id' => 6 )
        );
}

function language_type_data()
{
    return array(
            array('name' => 'English', 'id' => 1 ),
            array('name' => '中文', 'id' => 2 ),
            array('name' => '日本語', 'id' => 3 ),
            array('name' => 'Français', 'id' => 4 ),
            array('name' => '한국어', 'id' => 5 ),
            array('name' => 'Other', 'id' => 6 )
        );
}

function url_int_to_array($str)
{
    $tmp = explode(',', $str);

    $data = array();

    if (empty($tmp)) 
    {
        return $data;
    }

    foreach($tmp as $v) 
    {
        $id = (int) $v;

        if ($id > 0) 
        {
            $data[] = $v;
        }
    }

    return $data;
}

function pick_service_type_id($str)
{
    $data = array();

    $types = url_int_to_array($str);

    if (empty($types))
    {
        return $data;
    }

    $service_type = service_type_data();

    foreach($types as $v)
    {
        if (in_array($v, $service_type))
        {
            $data[] = $v;
        }
    }

    return $data;
}

function log_var($var)
{
   $vt = var_export($var, true);
   $var_str=strval($vt);
   
   log_message('error', $var_str);
}

function avatar_url($uid, $size = 0) 
{
    $src = '';

    $size_array = array(
        '120' => 'square.120',
        '100' => 'square.100',
        '50' => 'square.50',
        '0' => ''
    );

    if ($size)
    {
        $file_name = md5($uid)  . '.jpg!'. $size_array[$size];
    }
    else
    {
        $file_name = md5($uid)  . '.jpg';
    }

    $src = AVATAR_HOST . '/' . $file_name ;

    return $src;
}

define('EARTH_RADIUS', 6371);

//地球半径，平均半径为6371km
 /**
 *计算某个经纬度的周围某段距离的正方形的四个点
 *
 *@param lng float 经度
 *@param lat float 纬度
 *@param distance float 该点所在圆的半径，该圆与此正方形内切，默认值为0.5千米
 *@return array 正方形的四个点的经纬度坐标
 */
 function returnSquarePoint($lng, $lat,$distance = 0.5){

    $dlng =  2 * asin(sin($distance / (2 * EARTH_RADIUS)) / cos(deg2rad($lat)));
    $dlng = rad2deg($dlng);
    
    $dlat = $distance/EARTH_RADIUS;
    $dlat = rad2deg($dlat);
    
    return array(
                'left-top'=>array('lat'=>$lat + $dlat,'lng'=>$lng-$dlng),
                'right-top'=>array('lat'=>$lat + $dlat, 'lng'=>$lng + $dlng),
                'left-bottom'=>array('lat'=>$lat - $dlat, 'lng'=>$lng - $dlng),
                'right-bottom'=>array('lat'=>$lat - $dlat, 'lng'=>$lng + $dlng)
                );
 }

/* End of file MY_common.php */
/* Location: ./application/core/MY_common.php */