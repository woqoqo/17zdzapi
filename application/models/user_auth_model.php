<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class User_auth_model extends MY_Model {

	public $_table = 'user_auth';

	function __construct()
	{
		parent::__construct();
	}

}

// END User_auth_model class

/* End of file user_auth_model.php */
/* Location: ./application/model/user_auth_model.php */