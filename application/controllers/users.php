<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends MY_Controller 
{
	public function __construct() {
		parent::__construct();
	}
    
	public function search()
	{
		$this->load->library('Pages');
		$this->load->model('User_info_model');

		$key = $this->input->get_post('key', TRUE);
		$username = $this->input->get_post('username', TRUE);
        $user_type = (int) $this->input->get_post('user_type', TRUE);
        
		$filter = array();

		if($key)
		{
			$filter['place_info'] = $key;
		}
        
        if ($user_type) {
            $filter['user_type'] = $user_type;
        }

		$count = $this->User_info_model->count_all($filter);
		$offset = $this->pages->init_page($count);
		$limit = 10;

		$select = "uid, username, question_accepted, 
		question_answered, user_language, service_type, user_language";

		$data = array();
		$this->db->where('uid !=', $this->uid);

		if ($username) {
			$this->db->like('username', $username);
		}

		$this->db->select($select, FALSE);
		$data['users'] = $this->User_info_model->get_list('', $filter, $limit, $offset);
        
        if ($data['users'])
        {   
            foreach($data['users'] as $k => $v)
            {
                $data['users'][$k]->avatar =  avatar_url($v->uid, '100');
                $data['users'][$k]->normal_avatar =  avatar_url($v->uid);
            }  
        }
        
		$data['count_all'] = $count;

		show_ajax_success($data);
	}

	public function info()
	{
		$uid = (int) $this->input->get_post('uid');

		if($uid <= 0)
		{
			$uid = $this->uid;
		}

		$this->load->model('User_info_model');

		$filter = array(
			'uid' => $uid
		);

		$select = "uid,  username, user_type, nickname, content, gender, 
		country_id, country_name, city_id, city_name,
		idle, question_accepted, question_answered, user_language, service_type, 
        area_to_serve, receive_strangers_message";

		$user = $this->User_info_model->get_one($select, $filter);

		if(empty($user))
		{
			show_ajax_error('User does not exist', '30001');
		}
        
        if ($uid == $this->uid)
        {
            $this->load->model('Users_model');
            $select = 'phone, email';
            $filter = array('id' => $uid);
            $userinfo =  $this->Users_model->get_one($select, $filter);
            $user->phone = $userinfo->phone;
            $user->email = $userinfo->email;
            
            $this->load->model('User_service_area_model');            
            $service_filter = array('uid' => $this->uid);
            $user->service_area = $this->User_service_area_model->get_list('*', $service_filter);
        }

        $user->place_info = $user->country_name . ' ' . $user->city_name;
        $user->avatar = avatar_url($uid, '100');
        $user->normal_avatar = avatar_url($uid);

		$data = array();
		$data['user'] = $user;
        
        //is_follow
        $this->load->model('Contacts_model');

        $follow_filter = array(
            'uid' => $this->uid,
            'other_uid' => $uid
        );

        $count = $this->Contacts_model->count_all($follow_filter);
                
        $data['following'] = $count > 0 ? TRUE : FALSE;

		show_ajax_success($data);
	}
    
	public function service_type()
	{
		$service_type = service_type_data();

		$result = array(
			'service_type' => $service_type
		);

		show_ajax_success($result);
	}

	public function language_type()
	{
		$language_type = language_type_data();

		$result = array(
			'language_type' => $language_type
		);

		show_ajax_success($result);

	}
    
    public function filter()
    {
        $data = array(
            'type' =>  service_type_data(),
            'lang' => language_type_data()
        );
        
        show_ajax_success($data);
    }

	public function update_gender()
	{
		$this->load->model('User_info_model');

		$gender = (int) $this->input->post('gender');

		if($gender <0 || $gender > 2)
		{
			$gender = 0;
		}

		$updata_data = array(
			'gender' => $gender
		);

		$filter = array(
			'uid' => $this->uid
		);

		$this->User_info_model->update($updata_data, $filter);

		show_ajax_success();
	}
    
    public function update_content()
	{
		$this->load->model('User_info_model');

		$content = $this->input->post('content', TRUE);

		$updata_data = array(
			'content' => $content
		);

		$filter = array(
			'uid' => $this->uid
		);

		$this->User_info_model->update($updata_data, $filter);

		show_ajax_success();
	}

	public function update_idle()
	{
		$this->load->model('User_info_model');

		$idle = (int) $this->input->post('idle');
		$idle = ($idle === 0) ? 0 : 1; 

		$updata_data = array(
			'idle' => $idle
		);

		$filter = array(
			'uid' => $this->uid
		);

		$this->User_info_model->update($updata_data, $filter);

		show_ajax_success();
	}
    
    public function receive_strangers_message()
	{
		$this->load->model('User_info_model');

		$tmp = (int) $this->input->post('receive');
		$receive = ($tmp === 0) ? 0 : 1; 

		$updata_data = array(
			'receive_strangers_message' => $receive
		);

		$filter = array(
			'uid' => $this->uid
		);

		$this->User_info_model->update($updata_data, $filter);

		show_ajax_success();
	}


	public function update_avatar_name()
	{
		$this->load->model('User_info_model');

		$avatar = $this->input->post('avatar', true);

		$updata_data = array(
			'avatar' => $avatar
		);

		$filter = array(
			'uid' => $this->uid
		);

		$this->User_info_model->update($updata_data, $filter);

		show_ajax_success();
	}

	public function update_nickname()
	{
		$this->load->model('User_info_model');

		$rule = 'required';
		$this->form_validation->set_rules('nickname', 'Nickname', $rule);
		$nickname = $this->input->post('nickname');

		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '30003');
		}

		$updata_data = array(
			'nickname' => $nickname
		);

		$filter = array(
			'uid' => $this->uid
		);

		$this->User_info_model->update($updata_data, $filter);

		show_ajax_success();
	}

	public function update_language()
	{
		$id = $this->input->post('id');

		$language_id = '-';
		$language_name = '';
		$language_type = language_type_data();

		foreach($id as $v)
		{
			$v = (int) $v;

			-- $v;

			if ($v >= 0 && $v < 6) 
			{
				$language = $language_type[$v];
				$language_id .= $v . '-';
				$language_name .= $language['name'] . ' ';
			}
		}

		if(empty($language_id))
		{
			show_ajax_error('Id is not legitimate', '30009');
		}

		$filter = array(
			'uid' => $this->uid
		);

		$data = array(
			'user_language' => $language_name,
			'user_language_id' => $language_id,
		);

		$this->load->model('User_info_model');

		$this->User_info_model->update($data, $filter);

		show_ajax_success($data);
	}

	public function update_service()
	{
		$id = $this->input->post('id');

		$data = array();

        $service = service_type_data();
        
        $service_string = '';
        
		foreach($id as $v)
		{
			$v = (int) $v;

            $name = $service[$v-1]['name'];
            
			if ($v > 0 && $v <= 6) 
			{
				$data[] = array(
					'uid' => $this->uid,
					'service_type' => $v,
                    'service_type_name' =>  $name
				);
               
               $service_string .= ' ' .$name;
			}
		}

		if(count($data) == 0)
		{
			show_ajax_error('Id is not legitimate', '30009');
		}

		$filter = array(
			'uid' => $this->uid
		);

		$this->load->model('User_services_model');

		$this->User_services_model->delete($filter);
		$this->User_services_model->insert_batch($data);
        
        //userinfo
        $user_data = array(
            'service_type' =>  $service_string
        );
        $this->load->model('User_info_model');
		$this->User_info_model->update($user_data, $filter);

		show_ajax_success(array('service_string' => $service_string));
	}

	private function _add_service()
	{
		$this->load->model('User_services_model');

		$service = $this->_service_validation();

		$filter = array(
			'uid' => $uid,
			'service_type' => $service
		);

		$count = $this->User_services_model->count_all();

		if ($count == 0)
		{
			$insert_data = $filter;
			$this->User_services_model->insert($insert_data);
		}

		$result = array(
			'title' => 'users/add_service'
		);

		show_ajax_success($result);
	}

	public function delete_service()
	{
		$this->load->model('User_services_model');

		$service = $this->_service_validation();

		$filter = array(
			'uid' => $uid,
			'service_type' => $service
		);

		$this->User_services_model->delete($filter);

		$result = array(
			'title' => 'users/add_service'
		);

		show_ajax_success($result);
	}

	public function update_pw()
	{
		$this->form_validation->set_rules('original_password', 'Password', 'required|min_length[5]');		
		$this->form_validation->set_rules('new_password', 'New password', 'required|min_length[5]');		
		$this->form_validation->set_rules('confirm_password', 'Confirm new password', 'matches[new_password]');

		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '30005');
		}

		$new_password = $this->input->post('new_password');
		$password = $this->input->post('original_password');
		$password = hash_pwd($password);

		$this->load->model('Users_model');

		$select = 'password';
		$filter = array(
			'id' => $this->uid
		);

		$user = $this->Users_model->get_one($select, $filter);

		if($password != $user->password)
		{
			show_ajax_error('Password is not the same as the original', '30006');
		}

		$update_data = array(
			'password' => hash_pwd($new_password)
		);

		$this->Users_model->update($update_data, $filter);

		show_ajax_success();
	}

	public function integration()
	{
		$this->load->model('User_integration_log_model');

		$select = 'data_type';
		$filter = array('uid' => $this->uid);
		$this->User_integration_log_model->select_sum('integration', 'integration');
		$this->User_integration_log_model->group_by('data_type');

		$data = array();
		$data['integration'] = $this->User_integration_log_model->get_list($select, $filter);
		$data['title'] = 'users/integration';

		show_ajax_success($data);
	}

	public function integration_log()
	{
		$this->load->model('User_integration_log_model');

		$data_type = (int) $this->input->get('data_type');
		$select = 'content, integration, data_type, created';
		$filter = array('uid' => $this->uid, 'data_type' => $data_type);

		$data = array();
		$data['logs'] = $this->User_integration_log_model->get_list($select, $filter);
		$data['title'] = 'users/integration_log';

		show_ajax_success($data);
	}

	public function update_area()
	{
		$rule = 'required|is_natural_no_zero';
		$this->form_validation->set_rules('city_id', 'City id', $rule);

		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '30007');
		}

		$city_id = (int) $this->input->post('city_id');

		//area data
		$this->load->model('Areae_model');
		$areae = $this->Areae_model->parent_info($city_id);

		if (empty($areae))
		{
			show_ajax_error('Areae does not exist', '30008');
		}

		$data = array();

		if (isset($areae['grandpa_id']))
		{
			$data['continent_id'] = $areae['grandpa_id']->id;
			$data['continent_name'] = $areae['grandpa_id']->area_name;
		}

		if (isset($areae['parent_id']))
		{
			$data['country_id'] = $areae['parent_id']->id;
			$data['country_name'] = $areae['parent_id']->area_name;
		}

		$data['city_id'] = $areae['area_id']->id;
		$data['city_name'] = $areae['area_id']->area_name;
		$data['place_info'] = $data['continent_name'] .  $data['country_name'] . $data['city_name'];

		$this->load->model('User_info_model');
		$filter = array(
			'uid' => $this->uid
		);

		$this->User_info_model->update($data, $filter);

		show_ajax_success(array('area_info' => $data));
	}
    
    //
    
    public function area_to_serve()
    {
        $rule = 'required|is_natural_no_zero';
				
		$this->form_validation->set_rules('city_id', 'City id', $rule);


		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '30007');
		}

		$city_id = (int) $this->input->post('city_id');
                
		//area data
		$this->load->model('Areae_model');
		$areae = $this->Areae_model->parent_info($city_id);
        

		if (empty($areae))
		{
			show_ajax_error('Areae does not exist', '30008');
		}

		// for db data
		$data = array();

		if (isset($areae['grandpa_id']))
		{
			$data['continent_id'] = $areae['grandpa_id']->id;
			$data['continent_name'] = $areae['grandpa_id']->area_name;
		}

		if (isset($areae['parent_id']))
		{
			$data['country_id'] = $areae['parent_id']->id;
			$data['country_name'] = $areae['parent_id']->area_name;
		}

		$data['city_id'] = $areae['area_id']->id;
		$data['city_name'] = $areae['area_id']->area_name;
        
		$place_info = $data['continent_name'] . ' ' . $data['country_name'] . ' ' .$data['city_name'];


		$filter = array(
			'uid' => $this->uid
		);

		$this->load->model('User_info_model');
		$update_data = array(
			'area_to_serve' => $place_info
		);

		$this->User_info_model->update($update_data, $filter);
        
		$result = array(
			'place_info' => $place_info
		);

		show_ajax_success($result);

    }

	//服务地区
	public function service_area()
	{
		$rule = 'required|is_natural_no_zero';
				
//		$this->form_validation->set_rules('continent_id', 'Continent id', $rule);		
//		$this->form_validation->set_rules('country_id', 'Country id', $rule);

		$this->form_validation->set_rules('city_id', 'City id', $rule);
		$this->form_validation->set_rules('num', 'Num', $rule . '|less_than[4]');

		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '30007');
		}

		$num = (int) $this->input->post('num');
		$city_id = (int) $this->input->post('city_id');

		//area data
		$this->load->model('Areae_model');
		$areae = $this->Areae_model->parent_info($city_id);

		if (empty($areae))
		{
			show_ajax_error('Areae does not exist', '30008');
		}

		// for db data
		$data = array();

		if (isset($areae['grandpa_id']))
		{
			$data['continent_id'] = $areae['grandpa_id']->id;
			$data['continent_name'] = $areae['grandpa_id']->area_name;
		}

		if (isset($areae['parent_id']))
		{
			$data['country_id'] = $areae['parent_id']->id;
			$data['country_name'] = $areae['parent_id']->area_name;
		}

		$data['city_id'] = $areae['area_id']->id;
		$data['city_name'] = $areae['area_id']->area_name;
		
		//insert or update
		$this->load->model('User_service_area_model');

		$filter = array(
			'uid' => $this->uid,
			'num' => $num
		);

		$count = $this->User_service_area_model->count_all($filter);

		if ($count)
		{
			$this->User_service_area_model->update($data, $filter);
		}
		else
		{
			$data['uid'] = $this->uid;
			$data['num'] = $num;

			$this->User_service_area_model->insert($data);
		}

		$data['place_info'] = $data['continent_name'] . ' ' . $data['country_name'] . ' ' .$data['city_name'];

		//update user place info
		$place_info = ',';

		$filter = array(
			'uid' => $this->uid
		);
		$select = 'continent_name, country_name, city_name';
		$names = $this->User_service_area_model->get_list($select, $filter);

		foreach($names as $v)
		{
			$place_info .= $v->continent_name . ',';
			$place_info .= $v->country_name . ',';
			$place_info .= $v->city_name . ',';
		}

		$this->load->model('User_info_model');
		$update_data = array(
			'place_key' => $place_info
		);

		$this->User_info_model->update($update_data, $filter);

		$result = array(
			'area_info' => $data
		);

		show_ajax_success($result);
	}

	public function followed_service_accounts()
	{

		$this->load->model('Contacts_model');

        $filter = array(
            'uid' => $this->uid,
            'other_user_type' => 2
        );
        
        $select = 'other_uid, other_username, other_username_pinyin';
        
        $limit = 200;
        
        $users = $this->Contacts_model->get_list($select, $filter, $limit);

        $data = array();
        $data['users'] = $users;

        show_ajax_success($data);
	}
    
    public function contacts()
    {
        
        $this->load->model('Contacts_model');

        $filter = array(
            'uid' => $this->uid,
            'other_user_type' => 1
        );
        
        $select = 'other_uid, other_username, other_username_pinyin';
        
        $limit = 200;
        
        $users = $this->Contacts_model->get_list($select, $filter, $limit);

        $data = array();

        $data['users'] = array();

        foreach($users as $v)
        {
            $first = strtoupper(substr($v->other_username_pinyin, 0, 1));
            $ascii = ord($first);
            
            $v->avatar = avatar_url($v->other_uid, '100');
            $v->normal_avatar = avatar_url($v->other_uid);

            if ($ascii >= 48 && $ascii <= 57)
            {
                $first = '#';
            }
            
            $data['users'][$first][] = $v;
        }
        
        if (count($data['users'])) {
        	ksort($data['users']);
        }

 
//        $data['keys'] = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 
//            'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 
//            'x', 'y', 'z');
        
        show_ajax_success($data);
    }
    
    public function contacts_deal()
    {
        $uid = (int) $this->input->post('uid');

        if ($uid <= 0)
        {
            show_ajax_error('User does not exist', '30010');
        }

        $this->load->model('User_info_model');

        $user_select = 'uid, username, user_type';
        $user_filter = array('uid' => $uid);
        
        $user = $this->User_info_model->get_one($user_select, $user_filter);

        if (empty($user))
        {
            show_ajax_error('User does not exist', '30011');
        }

        $this->load->model('Contacts_model');

        $filter = array(
            'uid' => $this->uid,
            'other_uid' => $uid
        );

        $count_all = $this->Contacts_model->count_all($filter);
       
        $data = array();

        if ($count_all > 0)
        {
            $this->Contacts_model->delete($filter);

            $data['following'] = FALSE;
        }
        else
        {
        	$data['following'] = TRUE;

            $this->load->library('chinese');
            
            $username_pinyin = $this->chinese->cn2py($user->username);
            $dateline = dateline();

            $insert_data = array(
                'uid' => $this->uid,
                'username' => $this->username,
                'other_uid' => $uid,
                'other_username' => $user->username,
                'other_username_pinyin' => $username_pinyin,
                'other_user_type' => $user->user_type,
                'dateline' => $dateline
            );

            $this->Contacts_model->insert($insert_data);
        }

        show_ajax_success($data);
    }

    private function _service_validation()
	{
		$rule = 'required|is_natural_no_zero';
		$this->form_validation->set_rules('service', 'Service', $rule);
		$service = $this->input->post('service');

		if ($this->form_validation->run() === FALSE)
		{
			show_ajax_error(validation_errors(), '30004');
		}

		$service_type = service_type_data();

		if ( ! in_array($service, $service_type))
		{
			show_ajax_error(validation_errors(), '30004');
		}

		return $service;
	}
    
    
    public function assess_status()
    {
        $other_uid  = (int) $this->input->get_post('other_uid');
        
        if ($other_uid <= 0)
        {
            show_ajax_error('User does not exist', '30020');
        }
       
        $this->load->model('Assess_model');
        
       $filter = array(
           'other_uid' => $other_uid,
           'uid' => $this->uid
       ); 
       
       $result = array();
       
       $select = 'status';
       
       $result['assess'] = $this->Assess_model->get_one($select, $filter);
       
       show_ajax_success($result);
    }

    //评价
    public function send_assess()
    {
        $status = (int) $this->input->get_post('status');

        if ($status != -1)
        {
            $status = 1;
        }

        $other_uid = (int) $this->input->get_post('other_uid');

        if ($other_uid <= 0)
        {
            show_ajax_error('User does not exist', '30020');
        }

        $this->load->model('Assess_model');

        $filter = array(
            'other_uid' => $other_uid,
            'uid' => $this->uid
        );
        
        $select = 'status';
        $assess = $this->Assess_model->get_one($select, $filter);
        
        if($assess)
        {
            $updata_data = array();
             $updata_data['status'] =  ($assess->status == 0) ? $status : 0;
            
            $this->Assess_model->update($updata_data, $filter);
        } else {
            $insert_data = $filter;
            $insert_data['status'] = $status;

            $this->Assess_model->insert($insert_data);
        }

        show_ajax_success();
    }
}
